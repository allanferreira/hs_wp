<div class="depoimentosView">
	<div class="nav--menu-height"></div>

	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('depoimentos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hs-depoimentos-depoimentos" => [
				"titulo" => [
					"ptbr" => get_field('hs-depoimentos-depoimentos-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-depoimentos-depoimentos-enus-titulo', $post->ID)
				]
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"anos" =>[
			"ptbr" => "anos",
			"enus" => "years",
			"enau" => "años",
			"esco" => "años",
			"espe" => "años"
		],
		"content" => $content['hs-depoimentos-depoimentos'],
	], 'depoimentos', LANGUAGE, 'Views/HS/Depoimentos/Parts');
	?>

</div>

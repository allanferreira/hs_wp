<div class="aaustraliaView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('hc-ocanada-ptbr-titulo', $post->ID),
        "enus" => get_field('hc-ocanada-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-ocanada-ptbr-conteudo', $post->ID),
        "enus" => get_field('hc-ocanada-enus-conteudo', $post->ID)
      ]
    ],
    "imgdestaque" => get_field('hc-ocanada-imagem-destaque', $post->ID),
    "imgdetalhe" => get_field('hc-ocanada-imagem-detalhe', $post->ID),
    "detalhes" => [
      "ptbr" => get_field('hc-ocanada-ptbr-detalhes', $post->ID),
      "enus" => get_field('hc-ocanada-enus-detalhes', $post->ID)
    ],
    "porqueestudar" => [
      "titulo" => [
        "ptbr" => get_field('hc-ocanada-ptbr-porqueestudartitulo', $post->ID),
        "enus" => get_field('hc-ocanada-enus-porqueestudartitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-ocanada-ptbr-porqueestudarconteudo', $post->ID),
        "enus" => get_field('hc-ocanada-enus-porqueestudarconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
	$component = new Component;
	$component->create([
    "image" => $content['imgdestaque']
], 'banner', LANGUAGE, 'Views/HC/Ocanada/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "hc-ocanada-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
    "content" => $content['detalhes'],
    "image" => $content['imgdetalhe']
], 'detalhes', LANGUAGE, 'Views/HC/Ocanada/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['porqueestudar'],
    "class" => "hc-ocanada-porqueestudar"
  ], 'sectionText', LANGUAGE);
  ?>
</div>

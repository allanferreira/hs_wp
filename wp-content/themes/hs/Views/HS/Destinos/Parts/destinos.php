<section class="section--text hs-destinos-destinos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['hs-destinos-conteudo']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
	</div>

	<div class="content">
		<div class="item">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img class="img-response" src="<?php echo $state['content']['hs-destinos-imagem']['australia'];?>"/>
					</div>
					<div class="col-md-6">
						<div class="contenttext">
							<h1><?php echo $state['australia'][$lang];?></h1>
							<?php echo $state['content']['hs-destinos-conteudo']['australia'][$lang];?>
							<a href="<?php echo home_url();?>/ha">
								<button class="australia button--base"><?php echo $state['saibamais'][$lang];?></button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item gray">
			<div class="container">
				<div class="row">
					<div class="col-md-6 pull-right">
						<img class="img-response" src="<?php echo $state['content']['hs-destinos-imagem']['canada'];?>"/>
					</div>
					<div class="col-md-6">
						<div class="contenttext">
							<h1><?php echo $state['canada'][$lang];?></h1>
							<?php echo $state['content']['hs-destinos-conteudo']['canada'][$lang];?>
							<a href="<?php echo home_url();?>/hc">
								<button class="canada button--base"><?php echo $state['saibamais'][$lang];?></button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<img class="img-response" src="<?php echo $state['content']['hs-destinos-imagem']['england'];?>"/>
					</div>
					<div class="col-md-6">
						<div class="contenttext">
							<h1><?php echo $state['inglaterra'][$lang];?></h1>
							<?php echo $state['content']['hs-destinos-conteudo']['england'][$lang];?>
							<a href="<?php echo home_url();?>/he">
								<button class="england button--base"><?php echo $state['saibamais'][$lang];?></button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item gray">
			<div class="container">
				<div class="row">
					<div class="col-md-6 pull-right">
						<img class="img-response" src="<?php echo $state['content']['hs-destinos-imagem']['usa'];?>"/>
					</div>
					<div class="col-md-6">
						<div class="contenttext">
							<h1><?php echo $state['estadosunidos'][$lang];?></h1>
							<?php echo $state['content']['hs-destinos-conteudo']['usa'][$lang];?>
							<a href="<?php echo home_url();?>/hu">
								<button class="usa button--base"><?php echo $state['saibamais'][$lang];?></button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

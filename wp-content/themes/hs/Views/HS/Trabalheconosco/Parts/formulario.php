<section class="section--text hs-contato-formulario">
  <div class="container">
    <div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['hs-trabalheconosco-trabalheconosco']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
    <div class="row">
			<div class="col-md-8 col-md-offset-2">
        <div class="content">
            <?php //echo $state['hs-trabalheconosco-trabalheconosco']['conteudo'][$lang];?>
            <iframe class="iframeTrabalho" src="<?=$state['statics']['iframe'][$lang]?>"></iframe>
        </div>
        <!--div class="formulario" id="trabalheConosco">
            <div class="row">
                <div class="col-md-6">
                    <label>Nome</label>
                    <input v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('nome') }" type="text" name="nome"/>
                </div>
                <div class="col-md-6">
                    <label>Sobrenome</label>
                    <input v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('sobrenome') }" type="text" name="sobrenome"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Email:</label>
                    <input v-validate="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="text" name="email"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Data de Nascimento</label>
                    <input v-validate="'required|min:10|max:10'"  placeholder="DD/MM/AAAA" :class="{'input': true, 'is-danger': errors.has('datanasc') }" type="text" name="datanasc"/>
                </div>
                <div class="col-md-4">
                    <label>Telefone</label>
                    <input v-validate="'required|numeric|min:8|max:11'" :class="{'input': true, 'is-danger': errors.has('telefone') }" type="text" name="telefone"/>
                </div>
                <div class="col-md-4">
                    <label>Cidade de Residência</label>
                    <input v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('cidadeResidencia') }" type="text" name="cidadeResidencia"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Em qual país realizou intercãmbio?</label>
                    <div class="select" :class="{'input': true, 'is-danger': errors.has('qualPaisInter') }">
                        <select v-validate="'required|not_in:x'" name="qualPaisInter">
                            <option value="x">Selecione</option>
                            <option value="Austrália">Austrália</option>
                            <option value="Canadá">Canadá</option>
                            <option value="Estados Unidos">Estados Unidos</option>
                            <option value="Inglaterra">Inglaterra</option>
                            <option value="Outro">Outro</option>
                        </select>
                        <div class="h--clear"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Conte um pouco sobre sua experiência de Intercâmbio</label>
                    <textarea v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('expDeIntercambio') }" name="expDeIntercambio"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Quais países já visitou a turismo</label>
                    <textarea v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('paisesVisitados') }" name="paisesVisitados"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Qual o seu nível de inglês?</label>
                    <div class="select" :class="{'input': true, 'is-danger': errors.has('nivelIngles') }">
                        <select v-validate="'required|not_in:x'" name="nivelIngles">
                            <option value="x">Selecione</option>
                            <option value="Básico">Básico</option>
                            <option value="Intermediário">Intermediário</option>
                            <option value="Avançado">Avançado</option>
                            <option value="Fluente">Fluente</option>
                        </select>
                        <div class="h--clear"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <label>Como você conheceu a Hello Study?</label>
                    <div class="select" :class="{'input': true, 'is-danger': errors.has('comoconheceu') }">
                        <select v-validate="'required|not_in:x'" name="comoconheceu">
                            <option value="x">Selecione</option>
                            <option value="Indicação">Indicação</option>
                            <option value="Google">Google</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Feira de Intercâmbio">Feira de Intercâmbio</option>
                            <option value="Youtube">Youtube</option>
                            <option value="Blogs">Blogs</option>
                            <option value="Já viajou conosco">Já viajou conosco</option>
                            <option value="Outros">Outros</option>
                        </select>
                        <div class="h--clear"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>O que você espera desta oportunidade?</label>
                    <textarea v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('oQueEsperaOportunidade') }" name="oQueEsperaOportunidade"></textarea>
                </div>
            </div>
            <div class="col-md-12">
              <input @click="validate" class="button--base-big button--gd-hs1" type="submit" value="<?php echo $state['statics']['enviar'][$lang];?>"/>
            </div>
          </div>
        </div-->
      </div>
    </div>
  </div>
</section>

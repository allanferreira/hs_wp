<?php
class Component{

	function create(array $state, $component, $lang, $dir = null){
		set_query_var('state', $state);
		set_query_var($setLang, $lang);

		$template = ($dir) ? $dir.'/'.$component.'.php' : 'Components/'.$component.'.php';
		include(locate_template($template));
	}
}
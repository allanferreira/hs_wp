<section class="orcamentointercambiohaView">
  <div class="nav--menu-height-single"></div>
  <?php
	$content = [];
	$cats = [get_cat_ID('hc'), get_cat_ID('orcamentointercambio')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"titulo" => [
				"ptbr" => get_field('hc-orcamentointercambio-ptbr-titulo', $post->ID),
				"enus" => get_field('hc-orcamentointercambio-enus-titulo', $post->ID)
			],
			"form" => [
				"ptbr" => get_field('hc-orcamentointercambio-ptbr-form', $post->ID),
				"enus" => get_field('hc-orcamentointercambio-enus-form', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content
	], 'orcamentointercambio', LANGUAGE, 'Views/HC/Orcamentointercambio/Parts');
	?>
</section>

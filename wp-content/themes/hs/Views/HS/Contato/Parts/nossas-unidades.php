<section class="section--text hs-contato-nossasunidades">
  <div class="container">
    <div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['hs-contato-contato']['nossasunidades'][$lang];?></h1>
					<span></span>
				</div>
        <div class="content items">
          <div class="row">
            <?php
            $items = $state['hs-contato-nossasunidades'][$lang];
            $count = 0;
            foreach ($items as $key => $item):
              $count++;
            ?>
              <div class="item col-md-4">
                <h2><?php echo $item['hs-contato-nossasunidades-'.$lang.'-cidade'];?></h2>
                <strong><?php echo $item['hs-contato-nossasunidades-'.$lang.'-telefone'];?></strong>
                <p><?php echo $item['hs-contato-nossasunidades-'.$lang.'-endereco'];?></p>
              </div>
            <?php
            if($count==3){ echo '<div class="h--clear"></div>';$count=0;}
            endforeach;
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

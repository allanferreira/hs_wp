<div class="nav--menu hs">
	<nav class="nav--menu__bar">
		<div class="container">
			<div class="row">
				<div class="col-md-12 h--flex h--flex-center">
					<div class="h--flex-col">
						<div class="nav--menu__bar__icon">
							<?php
							$component = new Component;
							$component->create([], 'iconMenu', LANGUAGE);
							?>
						</div>
					</div>
					<div class="h--flex-col h--txt-center">
						<a href="<?php echo home_url();?>/">
							<img class="logo" src="<?php bloginfo('template_url');?>/img/hsLogo.png"/>
							<img class="logomin" src="<?php bloginfo('template_url');?>/img/hsLogomin.png"/>
						</a>
					</div>
					<div class="h--flex-col h--txt-right">
						<a href="<?php echo home_url();?>/orcamento-de-cursos">
							<button class="button--base orcamento">
								<?php echo $state['content']['orcamento'][$lang]; ?>
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<div class="h--relative">
		<?php
		$component = new Component;
		$component->create([
			"linguaDropdown" => "PT",
			"content" => [
				"sobre" => [
					"ptbr" => "Sobre",
					"espe" => "Sobre",
					"esco" => "Sobre",
					"enau" => "About",
					"enus" => "About"
				],
				"naoEncontrada" => [
					"ptbr" => "Não Encontrada",
					"espe" => "Não Encontrada",
					"esco" => "Não Encontrada",
					"enau" => "Page Not Found",
					"enus" => "Page Not Found"
				],
				"destinos" =>[
					"ptbr" => "Destinos",
					"espe" => "Destinos",
					"esco" => "Destinos",
					"enau" => "Destination",
					"enus" => "Destination"
				],
				"depoimentos" =>[
					"ptbr" => "Depoimentos",
					"espe" => "Depoimentos",
					"esco" => "Depoimentos",
					"enau" => "Witness",
					"enus" => "Witness"
				],
				"blog" =>[
					"ptbr" => "Blog",
					"espe" => "Blog",
					"esco" => "Blog",
					"enau" => "Blog",
					"enus" => "Blog"
				],
				"eventos" =>[
					"ptbr" => "Eventos",
					"espe" => "Eventos",
					"esco" => "Eventos",
					"enau" => "Event",
					"enus" => "Event"
				],
				"ciasaereas" =>[
					"ptbr" => "Cias Aéreas",
					"espe" => "Cias Aéreas",
					"esco" => "Cias Aéreas",
					"enau" => "Airlines",
					"enus" => "Airlines"
				],
				"contato" =>[
					"ptbr" => "Contato",
					"espe" => "Contato",
					"esco" => "Contato",
					"enau" => "Contact",
					"enus" => "Contact"
				]
			]
		], 'navMenuDrop', LANGUAGE);
		?>
	</div>
</div>

<section class="section--text hs-orcamentodecursos-orcamentodecursos">
  <div class="container">
    <div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['titulo'][$lang];?></h1>
					<span></span>
				</div>
        <div class="hub">
          <div class="au box formtarget" data-target="<?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['form']['au'][$lang];?>">
            <img class="titulo" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/titulo/au.png">
            <img class="bg" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/bg/au.jpg">
            <img class="arrow" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/arrow.png">
          </div>
          <div class="ca box formtarget" data-target="<?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['form']['ca'][$lang];?>">
            <img class="titulo" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/titulo/ca.png">
            <img class="bg" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/bg/ca.jpg">
            <img class="arrow" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/arrow.png">
          </div>
          <div class="en box formtarget" data-target="<?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['form']['en'][$lang];?>">
            <img class="titulo" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/titulo/en.png">
            <img class="bg" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/bg/en.jpg">
            <img class="arrow" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/arrow.png">
          </div>
          <div class="us box formtarget" data-target="<?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['form']['us'][$lang];?>">
            <img class="titulo" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/titulo/us.png">
            <img class="bg" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/bg/us.jpg">
            <img class="arrow" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/arrow.png">
          </div>
          <div class="box formtarget" data-target="<?php echo $state['content']['hs-orcamentodecursos-orcamentodecursos']['form']['naosei'][$lang];?>">
            <img class="titulo" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/titulo/naosei-<?php echo $lang;?>.png">
            <img class="bg" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/bg/naosei.jpg">
            <img class="arrow" src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/arrow.png">
          </div>
          <div class="h--clear"></div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container">
    <div class="row">
  		<div class="col-md-12">
        <div class="passagemaerea">
          Para orçamento de passagens aéreas,
          <a href="<?php echo home_url();?>/orcamento-de-passagens-aereas">
            clique aqui
            <img src="<?php echo bloginfo('template_url');?>/img/hs/orcamentodecursos/icon.png">
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="formresult formulario"></div>
</section>

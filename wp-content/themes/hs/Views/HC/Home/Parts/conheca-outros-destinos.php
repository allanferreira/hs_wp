<section class="section--text section--ha conheca-outros-destinosSection">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="title">
					<h1><?php echo $state['titulo'][$lang];?></h1>
					<span></span>
				</div>
      </div>
			<div class="col-md-8">
				<div class="outrospaises">
					<?php
					$component = new Component;
					$component->create([
						"saibamais" => [
							"ptbr" => "Saiba mais",
							"enus" => "Saiba mais"
						]
					], 'banner-box', LANGUAGE);
					?>
				</div>
			</div>
    </div>
  </div>
</section>

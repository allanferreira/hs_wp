<section class="sobrehaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$content = [];
	$cats = [get_cat_ID('hu'), get_cat_ID('sobre')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"image" => [
				"ptbr" => get_field('hu-sobre-ptbr-imagem', $post->ID),
				"enus" => get_field('hu-sobre-enus-imagem', $post->ID)
			],
			"titulo" => [
				"ptbr" => get_field('hu-sobre-ptbr-titulo', $post->ID),
				"enus" => get_field('hu-sobre-enus-titulo', $post->ID)
			],
			"conteudo" => [
				"ptbr" => get_field('hu-sobre-ptbr-conteudo', $post->ID),
				"enus" => get_field('hu-sobre-enus-conteudo', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>
    <?php
    $component = new Component;
    $component->create([
        "nostatic" => "nostatic",
        "image" => $content['image']
    ], 'banner-static', 'ptbr');
    ?>
<?php
$component = new Component;
$component->create([
  "content" => $content,
  "class" => "ha-sobre"
], 'sectionText', LANGUAGE);
?>
</section>

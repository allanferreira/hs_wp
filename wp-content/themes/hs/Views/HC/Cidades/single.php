<div class="cidadesSingleView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "image" => get_field('type-cidades-imagem', $post->ID),
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-titulo', $post->ID),
        "enus" => get_field('type-cidades-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-conteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-conteudo', $post->ID)
      ]
    ],
    "historia" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-historiatitulo', $post->ID),
        "enus" => get_field('type-cidades-enus-historiatitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-historiaconteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-historiaconteudo', $post->ID)
      ]
    ],
    "clima" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-climatitulo', $post->ID),
        "enus" => get_field('type-cidades-enus-climatitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-climaconteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-climaconteudo', $post->ID)
      ]
    ],
    "religiao" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-religiaotitulo', $post->ID),
        "enus" => get_field('type-cidades-enus-religiaotitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-religiaoconteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-religiaoconteudo', $post->ID)
      ]
    ],
    "transporte" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-transportetitulo', $post->ID),
        "enus" => get_field('type-cidades-enus-transportetitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-transporteconteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-transporteconteudo', $post->ID)
      ]
    ],
    "atracoes" => [
      "titulo" => [
        "ptbr" => get_field('type-cidades-ptbr-atracoestitulo', $post->ID),
        "enus" => get_field('type-cidades-enus-atracoestitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('type-cidades-ptbr-atracoesconteudo', $post->ID),
        "enus" => get_field('type-cidades-enus-atracoesconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
  $component = new Component;
  $component->create([
    "image"=> $content['image']
], 'banner', LANGUAGE, 'Views/HC/Cidades/Partssingle');
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "ha-single-cidades-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['historia'],
    "class" => "ha-single-cidades-historia"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['clima'],
    "class" => "ha-single-cidades-clima"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['religiao'],
    "class" => "ha-single-cidades-religiao gray"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['transporte'],
    "class" => "ha-single-cidades-transporte"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['atracoes'],
    "class" => "ha-single-cidades-atracoes"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
		"statics" => [
			"facaorcamento" => [
				"ptbr"  => "Faça um orçamento",
				"enus"  => "Faça um orçamento",
				"eses"  => "Faça um orçamento"
			]
		]
  ], 'facaorcamento', LANGUAGE, 'Views/HC/Cidades/Partssingle');
  ?>
</div>

<div class="blogSingleView">
	<div class="nav--menu-height"></div>
  <?php
  the_post();
  $content = [
    "titulo" => [
      "ptbr" => get_field('type-blog-ptbr-titulo', $post->ID),
      "enus" => get_field('type-blog-enus-titulo', $post->ID)
    ],
    "conteudo" => [
      "ptbr" => get_field('type-blog-ptbr-conteudo', $post->ID),
      "enus" => get_field('type-blog-enus-conteudo', $post->ID)
    ],
    "imagem" => get_field('type-blog-imagem', $post->ID)
  ];
  ?>

  <?php
  $component = new Component;
  $component->create([
  	"content" => $content,
  ], 'artigo', LANGUAGE, 'Views/HS/Blog/Partssingle');
  ?>

  <?php
  $component = new Component;
  $component->create([
    "statics" => [
      "postsrelacionados" => [
        "ptbr"=>"Posts Relacionados",
        "enus"=>"Posts Relacionados"
      ],
      "leiamais" => [
        "ptbr"=>"Leia mais",
        "enus"=>"Read more"
      ],
			"current_id" => [$post->ID]
    ]
  ], 'posts-relacionados', LANGUAGE, 'Views/HS/Blog/Partssingle');
  ?>
</div>

<div class="parceirosView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('he'), get_cat_ID('parceiros')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"he-parceiros-titulo" => [
  			"parceiros" => [
  				"ptbr" => get_field('he-parceiros-parceiros-ptbr-titulo', $post->ID),
  				"enus" => get_field('he-parceiros-parceiros-enus-titulo', $post->ID)
  			],
  			"escolas" => [
  				"ptbr" => get_field('he-parceiros-escolas-ptbr-titulo', $post->ID),
  				"enus" => get_field('he-parceiros-escolas-enus-titulo', $post->ID)
  			]
  		],
      "he-parceiros-marcas" => [
        "parceiros" => get_field('he-parceiros-parceiros-marcas', $post->ID),
  			"escolas" => get_field('he-parceiros-parceiros-escolas', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'parceiros', LANGUAGE, 'Views/HE/Parceiros/Parts');
  ?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'escolas', LANGUAGE, 'Views/HE/Parceiros/Parts');
	?>
</div>

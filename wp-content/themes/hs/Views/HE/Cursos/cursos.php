<section class="servicoshaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$content = [];
	$cats = [get_cat_ID('he'), get_cat_ID('cursos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"page" => [
  			"titulo" => [
  				"ptbr" => get_field('he-cursos-ptbr-titulo', $post->ID),
  				"enus" => get_field('he-cursos-enus-titulo', $post->ID)
  			],
  			"conteudo" => [
  				"ptbr" => get_field('he-cursos-ptbr-conteudo', $post->ID),
  				"enus" => get_field('he-cursos-enus-conteudo', $post->ID)
  			]
  		],
      "items" => [
        "ptbr" => get_field('he-cursos-ptbr-item', $post->ID),
        "enus" => get_field('he-cursos-enus-item', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['page'],
    "class" => "ha-servicos"
  ], 'sectionTextSimple', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content['items']
	], 'items', LANGUAGE, 'Views/HE/Cursos/Parts');
	?>
      <div class="ha-solicite-um-orcamento">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a href="<?php echo home_url();?>/he/orcamento-de-cursos/?en">
            <button class="button--base-big button--gd-ha1">Solicite um orçamento</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

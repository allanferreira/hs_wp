<nav class="nav-mobile">
  <div class="nav-mobile__close">
    x
  </div>
  <div class="nav-mobile__destinos">
    <div class="flag top"></div>
    <div class="flag bottom"></div>
    <ul class="disable">
      <li class="hs color1 <?php if($state['tela']=='hs'){ echo 'active';}?>" data-aba="hs">
        Hello Study
        <div class="hs bgcolor1 enable">Hello Study</div>
      </li>
      <li class="ha color1 <?php if($state['tela']=='ha'){ echo 'active';}?>" data-aba="ha">
        Australia
        <div class="ha bgcolor1 enable">Australia</div>
      </li>
      <li class="hc color1 <?php if($state['tela']=='hc'){ echo 'active';}?>" data-aba="hc">
        Canada
        <div class="hc bgcolor1 enable">Canada</div>
      </li>
      <li class="he color1 <?php if($state['tela']=='he'){ echo 'active';}?>" data-aba="he">
        England
        <div class="he bgcolor1 enable">England</div>
      </li>
      <li class="hu color1 <?php if($state['tela']=='hu'){ echo 'active';}?>" data-aba="hu">
        Usa
        <div class="hu bgcolor1 enable">Usa</div>
      </li>
    </ul>
  </div>
  <div class="hs bgcolor1 menu menuhs <?php if($state['tela']=='hs'){ echo 'active';}?>">
    <a href="<?php echo home_url();?>">
      <img class="logo" src="<?php echo bloginfo('template_url');?>/img/hsLogo.png" alt="Hello Study">
    </a>
    <ul class="nav-mobile__menu">
      <li>
        <a href="<?php echo home_url();?>/sobre">
          <span class="title">Sobre</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/destino">
          <span class="title">Destinos</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/depoimentos">
          <span class="title">Depoimentos</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/blog">
          <span class="title">Blog</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/eventos">
          <span class="title">Eventos</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/ciasaereas">
          <span class="title">Cias Aéreas</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/trabalhe-conosco">
          <span class="title">Trabalhe conosco</span>
        </a>
      </li>
      <li>
        <a href="<?php echo home_url();?>/contato">
          <span class="title">Contato</span>
        </a>
      </li>
    </ul>
    <div class="nav-mobile__orcamento">
      <a href="<?php echo home_url();?>/orcamento-de-cursos">
        <button class="orcamento button--base">Orçamento</button>
      </a>
    </div>
  </div>
  <div class="ha bgcolor1 menu menuha <?php if($state['tela']=='ha'){ echo 'active';}?>">
    <a href="<?php echo home_url();?>/ha">
      <img class="logo" src="<?php echo bloginfo('template_url');?>/img/mobile/ha.png" alt="Hello Australia">
    </a>
    <ul class="nav-mobile__menu">
      <li>
        <span class="title">Hello Australia</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/ha/sobre">Sobre</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/servicos">Serviços</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/parceiros">Parceiros</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Estude na Australia</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/ha/aaustralia">A Austrália</a>
          </li>
          <li>
              <a href="<?php echo home_url();?>/ha/cidades">Cidades</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/cursos">Cursos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/acomodacoes">Acomodações</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Informações</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/ha/vistosedocumentos">Vistos & Documentos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/trabalho">Trabalho</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Promoções</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/ha/promocoes-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/promocoes-passagens">Passagens</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Orçamento</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/ha/orcamento-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/ha/orcamento-passagens">Passagens</a>
          </li>
        </ul>
      </li>
    </ul>
    <div class="nav-mobile__orcamento">
      <a href="<?php echo home_url();?>/orcamento-de-cursos">
        <button class="orcamento button--base">Orçamento</button>
      </a>
    </div>
  </div>
  <div class="hc bgcolor1 menu menuhc <?php if($state['tela']=='hc'){ echo 'active';}?>">
    <a href="<?php echo home_url();?>/hc">
      <img class="logo" src="<?php echo bloginfo('template_url');?>/img/mobile/hc.png" alt="Hello Canada">
    </a>
    <ul class="nav-mobile__menu">
      <li>
        <span class="title">Hello Canada</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hc/sobre">Sobre</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/servicos">Serviços</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/parceiros">Parceiros</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Estude no Canada</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hc/ocanada">O Canada</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/cursos">Cursos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/acomodacoes">Acomodações</a>
          </li>
          <li>
              <a href="<?php echo home_url();?>/hc/cidades">Cidades</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Informações</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hc/vistosedocumentos">Vistos & Documentos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/trabalho">Trabalho</a>
        </li>
        </ul>
      </li>
      <li>
        <span class="title">Promoções</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hc/promocoes-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/promocoes-passagens">Passagens</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Orçamento</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hc/orcamento-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hc/orcamento-passagens">Passagens</a>
          </li>
        </ul>
      </li>
    </ul>
    <div class="nav-mobile__orcamento">
      <a href="<?php echo home_url();?>/orcamento-de-cursos">
        <button class="orcamento button--base">Orçamento</button>
      </a>
    </div>
  </div>
  <div class="hu bgcolor1 menu menuhu <?php if($state['tela']=='hu'){ echo 'active';}?>">
    <a href="<?php echo home_url();?>/hu">
      <img class="logo" src="<?php echo bloginfo('template_url');?>/img/mobile/hu.png" alt="Hello USA">
    </a>
    <ul class="nav-mobile__menu">
      <li>
        <span class="title">Hello USA</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hu/sobre">Sobre</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/servicos">Serviços</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/parceiros">Parceiros</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Estude nos Estados Unidos</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hu/osestadosunidos">Os Estados Unidos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/cursos">Cursos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/acomodacoes">Acomodações</a>
          </li>
          <li>
              <a href="<?php echo home_url();?>/hu/cidades">Cidades</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Informações</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hu/vistosedocumentos">Vistos & Documentos</a>
          </li>
          <!--li>
            <a href="<?php echo home_url();?>/hu/trabalho">Trabalho</a>
        </li-->
        </ul>
      </li>
      <li>
        <span class="title">Promoções</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hu/promocoes-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/promocoes-passagens">Passagens</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Orçamento</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/hu/orcamento-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/hu/orcamento-passagens">Passagens</a>
          </li>
        </ul>
      </li>
    </ul>
    <div class="nav-mobile__orcamento">
      <a href="<?php echo home_url();?>/orcamento-de-cursos">
        <button class="orcamento button--base">Orçamento</button>
      </a>
    </div>
  </div>
  <div class="he bgcolor1 menu menuhe <?php if($state['tela']=='he'){ echo 'active';}?>">
    <a href="<?php echo home_url();?>/he">
      <img class="logo" src="<?php echo bloginfo('template_url');?>/img/mobile/he.png" alt="Hello England">
    </a>
    <ul class="nav-mobile__menu">
      <li>
        <span class="title">Hello England</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/he/sobre">Sobre</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/servicos">Serviços</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/parceiros">Parceiros</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Estude na Inglaterra</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/he/ainglaterra">A Inglaterra</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/cursos">Cursos</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/acomodacoes">Acomodações</a>
          </li>
          <li>
              <a href="<?php echo home_url();?>/he/cidades">Cidades</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Informações</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/he/vistosedocumentos">Vistos & Documentos</a>
          </li>
          <!--li>
            <a href="<?php echo home_url();?>/he/trabalho">Trabalho</a>
        </li-->
        </ul>
      </li>
      <li>
        <span class="title">Promoções</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/he/promocoes-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/promocoes-passagens">Passagens</a>
          </li>
        </ul>
      </li>
      <li>
        <span class="title">Orçamento</span>
        <span class="arrow"></span>
        <ul>
          <li>
            <a href="<?php echo home_url();?>/he/orcamento-intercambio">Intercâmbio</a>
          </li>
          <li>
            <a href="<?php echo home_url();?>/he/orcamento-passagens">Passagens</a>
          </li>
        </ul>
      </li>
    </ul>
    <div class="nav-mobile__orcamento">
      <a href="<?php echo home_url();?>/orcamento-de-cursos">
        <button class="orcamento button--base">Orçamento</button>
      </a>
    </div>
  </div>
</nav>

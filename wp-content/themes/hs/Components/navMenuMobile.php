<nav class="nav-mobile-bar <?=$state['tela']?>">
  <div class="nav-mobile-open">
    <div class="icon--menu">
    		<span></span>
    		<span></span>
    		<span></span>
    	</div>
  </div>
  <div class="logo logo<?=$state['tela']?>">
    <a href="<?php echo home_url();?>/<?php if($state['tela'] != 'hs'){ echo $state['tela'];}?>">
      <img src="<?php echo bloginfo('template_url');?>/img/<?=$state['tela']?>Logomin.png">
    </a>
  </div>
</div>

<?php
$component = new Component;
$component->create([
  'tela' => $state['tela']
], 'navMobile', LANGUAGE);
?>

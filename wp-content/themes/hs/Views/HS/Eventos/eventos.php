<div class="eventosView">
	<div class="nav--menu-height"></div>

	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('eventos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"titulo" => [
				"ptbr" => get_field('hs-eventos-eventos-ptbr-titulo', $post->ID),
				"enus" => get_field('hs-eventos-eventos-enus-titulo', $post->ID)
			],
			"local" => [
				"ptbr"  => "Local",
				"enus"  => "Local",
				"eses"  => "Sitio"
			],
			"horario" => [
				"ptbr"  => "Horário",
				"enus"  => "Schedule",
				"eses"  => "Tiempo"
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'eventos', LANGUAGE, 'Views/HS/Eventos/Parts');
	?>

</div>

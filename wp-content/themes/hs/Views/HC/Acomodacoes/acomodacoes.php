<div class="acamodacoesView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "image" => get_field('hc-acomodacoes-imagem', $post->ID),
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-titulo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-conteudo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-conteudo', $post->ID)
      ]
    ],
    "homestay" => [
      "titulo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-homestaytitulo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-homestaytitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-homestayconteudo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-homestayconteudo', $post->ID)
      ]
    ],
    "studenthouse" => [
      "titulo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-studenthousetitulo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-studenthousetitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-studenthouseconteudo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-studenthouseconteudo', $post->ID)
      ]
    ],
    "sharehouse" => [
      "titulo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-sharehousetitulo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-sharehousetitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-sharehouseconteudo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-sharehouseconteudo', $post->ID)
      ]
    ],
    "backpacker" => [
      "titulo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-backpackertitulo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-backpackertitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hc-acomodacoes-ptbr-backpackerconteudo', $post->ID),
        "enus" => get_field('hc-acomodacoes-enus-backpackerconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "ha-acomodacoes-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['homestay'],
    "class" => "ha-acomodacoes-homestay gray"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['studenthouse'],
    "class" => "ha-acomodacoes-studenthouse"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sharehouse'],
    "class" => "ha-acomodacoes-sharehouse gray"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['backpacker'],
    "class" => "ha-acomodacoes-backpacker"
  ], 'sectionText', LANGUAGE);
  ?>

</div>

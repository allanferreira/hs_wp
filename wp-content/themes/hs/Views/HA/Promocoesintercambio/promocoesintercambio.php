<section class="promocoesinterhaView">
  <div class="nav--menu-height-single"></div>
  <?php
  $content = [];
  $cats = [get_cat_ID('ha'), get_cat_ID('promocoesintercambio')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "ha-promocoesintercambio-texto" => [
          "ptbr" => get_field('ha-promocoesintercambio-ptbr-texto', $post->ID),
          "enus" => get_field('ha-promocoesintercambio-enus-texto', $post->ID)
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>
  <?php
	$component = new Component;
	$component->create([
    "texto" => $content["ha-promocoesintercambio-texto"],
    "static" => [
      "titulo" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ]
    ]
	], 'item', LANGUAGE, 'Views/HA/Promocoesintercambio/Parts');
	?>
</section>

<div class="ciasaereasView">
  <div class="nav--menu-height"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('ciasaereas')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hs-ciasaereas-sobre" => [
  			"titulo" => [
  				"ptbr" => get_field('hs-ciasaereas-sobre-ptbr-titulo', $post->ID),
  				"enus" => get_field('hs-ciasaereas-sobre-enus-titulo', $post->ID)
  			],
  			"conteudo" => [
  				"ptbr" => get_field('hs-ciasaereas-sobre-ptbr-conteudo', $post->ID),
  				"enus" => get_field('hs-ciasaereas-sobre-enus-conteudo', $post->ID)
  			]
  		],
			"hs-ciasaereas-marcas" => [
        "au" => [
          "marca" => get_field('hs-ciasaereas-marcas-au', $post->ID),
          "titulo" => [
            "ptbr" => 'Austrália',
            "espe" => 'Austrália',
            "esco" => 'Austrália',
            "enus" => 'Australia'
				  ]
				],
        "ca" => [
          "marca" => get_field('hs-ciasaereas-marcas-ca', $post->ID),
          "titulo" => [
            "ptbr" => 'Canadá',
            "espe" => 'Canadá',
            "esco" => 'Canadá',
            "enus" => 'Canada'
				  ]
				],
        "en" => [
          "marca" => get_field('hs-ciasaereas-marcas-en', $post->ID),
          "titulo" => [
            "ptbr" => 'Inglaterra',
            "espe" => 'Inglaterra',
            "esco" => 'Inglaterra',
            "enus" => 'England'
				  ]
				],
        "eu" => [
          "marca" => get_field('hs-ciasaereas-marcas-eu', $post->ID),
          "titulo" => [
            "ptbr" => 'Estados Unidos',
            "espe" => 'Estados Unidos',
            "esco" => 'Estados Unidos',
            "enus" => 'United States'
				  ]
				]
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-ciasaereas-sobre'],
		"class" => "hs-ciasaereas-sobre"
	], 'sectionTextSimple', LANGUAGE);
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-ciasaereas-marcas'],
	], 'marcas', LANGUAGE, 'Views/HS/Ciasaereas/Parts');
	?>
</div>

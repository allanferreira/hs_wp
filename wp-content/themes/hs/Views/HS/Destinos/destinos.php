<div class="destinosView">
	<div class="nav--menu-height"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('destinos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();
		$content = [

			"hs-destinos-imagem" => [
				"australia" =>  get_field('hs-destinos-imagem-australia', $post->ID),
				"canada" => get_field('hs-destinos-imagem-canada', $post->ID),
				"england" => get_field('hs-destinos-imagem-england', $post->ID),
				"usa" => get_field('hs-destinos-imagem-usa', $post->ID)
			],
			"hs-destinos-conteudo" => [
				"titulo" => [
					"ptbr" => get_field('hs-destinos-conteudo-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-destinos-conteudo-enus-titulo', $post->ID)
				],
				"australia" => [
					"ptbr" => get_field('hs-destinos-conteudo-ptbr-australia', $post->ID),
					"enus" => get_field('hs-destinos-conteudo-enus-australia', $post->ID)
				],
				"canada" => [
					"ptbr" => get_field('hs-destinos-conteudo-ptbr-canada', $post->ID),
					"enus" => get_field('hs-destinos-conteudo-enus-canada', $post->ID)
				],
				"england" => [
					"ptbr" => get_field('hs-destinos-conteudo-ptbr-england', $post->ID),
					"enus" => get_field('hs-destinos-conteudo-enus-england', $post->ID)
				],
				"usa" => [
					"ptbr" => get_field('hs-destinos-conteudo-ptbr-usa', $post->ID),
					"enus" => get_field('hs-destinos-conteudo-enus-usa', $post->ID)
				]
			]
		];

	endwhile; wp_reset_postdata();

	$component = new Component;
	$component->create([
		"australia" => [
			"ptbr" => "Austrália",
			"enus" => "Australia"
		],
		"estadosunidos" => [
			"ptbr" => "Estados Unidos",
			"enus" => "Estados Unidos"
		],
		"inglaterra" => [
			"ptbr" => "Inglaterra",
			"enus" => "Inglaterra"
		],
		"canada" => [
			"ptbr" => "Canadá",
			"enus" => "Canada"
		],
		"saibamais" => [
			"ptbr" => "Saiba mais",
			"enus" => "Read more"
		],
		"content" => $content,
	], 'destinos', LANGUAGE, 'Views/HS/Destinos/Parts');
	?>
</div>

const dropdownlang = $('.dropdownlang')
dropdownlang.click(function(){
	$(this).toggleClass('open')
})
dropdownlang.find('li').click(function(){
	const val = $(this).html()
	$(this).parent().parent().find('.dropdownlang__view__current').html(val)
})
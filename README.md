# HS WP

###### powered by
[![Magor](https://cldup.com/5Mx8Oituad.png)](http://magor.com.br)

>Esse é um projeto em [Wordpress].


### Rodar
Instalar dependências

```sh
$ npm i
$ gulp build
```
### Desenvolver
Para desenvolver é necessário usar [Gulp] para montar a pasta `dist` a partir do `src`

```sh
$ gulp watch
```

### License
MIT

[Jquery]: <https://jquery.com/>
[Twitter Bootstrap]: <http://twitter.github.com/bootstrap>
[Webpack]: <https://webpack.js.org/>
[Wordpress]: <https://wordpress.org>
[Gulp]: <http://gulpjs.com>

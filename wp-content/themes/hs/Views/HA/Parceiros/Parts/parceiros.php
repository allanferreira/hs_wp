<section class="section--text ha-parceiros-parceiros">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['ha-parceiros-titulo']['parceiros'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
          <section class="ha-parceiros-marcas">
            <div class="paises">
              <div class="pais">
                <ul class="marcas">
                  <?php foreach ($state['content']['ha-parceiros-marcas']['parceiros'] as $key => $imagem):?>
                    <li class="marca">
                      <?php if(empty($imagem[caption])): ?>
                        <div class="h--table">
                          <div class="h--table-cell">
                              <img src="<?php echo $imagem['sizes']['marcas'];?>"/>
                          </div>
                        </div>
                      <?php else: ?>
                        <a style="height: inherit;" href="<?php echo $imagem[caption];?>" target="_blank">
                          <div class="h--table">
                            <div class="h--table-cell">
                                <img src="<?php echo $imagem['sizes']['marcas'];?>"/>
                            </div>
                          </div>
                        </a>
                      <?php endif;?>
                    <?php endforeach;?>
                  </li>
                </ul>
              </div>
            </div>
          </section>
				</div>
			</div>
		</div>
	</div>
</section>

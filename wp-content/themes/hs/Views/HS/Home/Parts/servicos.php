<section class="section--default servicosSection">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="title">
					<h1><?php echo $state['content']['servicosTitle'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
					<?php echo $state['content']['servicosContent'][$lang];?>
				</div>
			</div>
			<div class="col-md-8 col-sm-12">
				<div class="h--flex-center icon-pack">
					<div class="chavesIcon">
						<?php
						$component = new Component;
						$component->create([], 'iconChaves', LANGUAGE);
						?>
					</div>
					<ul class="icons h--flex-col">
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconCurso', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosCurso'][$lang];?></pre>
							</span>
						</li>
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconAcomodacao', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosAcomodacao'][$lang];?></pre>
							</span>
						</li>
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconVisco', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosVisto'][$lang];?></pre>
							</span>
						</li>
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconPassagemAerea', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosPassagemAerea'][$lang];?></pre>
							</span>
						</li>
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconAssistenciaViagem', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosAssistenciaViagem'][$lang];?></pre>
							</span>
						</li>
						<li>
							<?php
							$component = new Component;
							$component->create([], 'iconCartaoMultiMoeda', LANGUAGE);
							?>
							<span>
								<pre><?php echo $state['content']['servicosCartaoMultiMoeda'][$lang];?></pre>
							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="button--base-big--section">
		<a href="<?php echo home_url();?>/sobre"><button class="button--base-big button--gd-hs1"><?php echo $state['content']['saibamais'][$lang];?></button></a>
	</div>
</section>

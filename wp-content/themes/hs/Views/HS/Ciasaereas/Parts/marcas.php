<section class="hs-ciasaereas-marcas">
  <div class="container">
    <div class="row">
      <div class="col-md-12 paises">
        <?php foreach ($state['content'] as $key => $pais):?>
          <div class="pais">
            <div class="title">
              <h1><?php echo $pais['titulo'][$lang];?></h1>
              <span class="<?php echo $key;?>"></span>
            </div>
            <ul class="marcas">
              <?php foreach ($state['content'][$key]['marca'] as $key => $imagem):?>
                <li class="marca">
                  <div class="h--table">
                    <div class="h--table-cell">
                      <img src="<?php echo $imagem['sizes']['marcas'];?>"/>
                    </div>
                  </div>
                </li>
              <?php endforeach;?>
            </ul>
          </div>
        <?php endforeach;?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <a href="<?php echo home_url();?>/orcamento-de-passagens-aereas/">
          <button class="button--base-big button--gd-hs1">Solicite um orçamento</button>
        </a>
      </div>
    </div>
  </div>
</section>

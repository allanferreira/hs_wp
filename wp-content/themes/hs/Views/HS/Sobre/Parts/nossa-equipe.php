<section class="section--text hs-sobre-nossa-equipe">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="gallery--list">
					<div class="gallery--list__icon iconprev"></div>
					<div class="gallery--list__icon iconnext"></div>
					<div class="owl-carousel owl-theme">
						<?php
						$cats = [get_cat_ID('hs'), get_cat_ID('sobre')];
						$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
						while ( $query->have_posts() ): $query->the_post();
							$images = get_field('hs-sobre-nossa-equipe-ptbr-galeria');
							foreach ($images as $image):
						?>

							<div class="item">
								<img src="<?php echo $image['sizes']['equipe'];?>"/>
								<h2><?php echo $image['alt'];?></h2>
								<h3><?php echo $image['description'];?></h3>
							</div>

						<?php endforeach; endwhile; wp_reset_postdata();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

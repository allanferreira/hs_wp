<div class="blogView">
	<div class="nav--menu-height"></div>

	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('blog')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hs-blog-blog" => [
				"titulo" => [
					"ptbr" => get_field('hs-blog-blog-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-blog-blog-enus-titulo', $post->ID)
				]
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-blog-blog'],
	], 'blog', LANGUAGE, 'Views/HS/Blog/Parts');
	?>

</div>

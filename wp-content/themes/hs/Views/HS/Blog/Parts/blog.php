<section class="section--text hs-blog-blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
	</div>

	<div class="content">


		<?php
		$lista = 5;
		$args = array(
			'post_type' => 'blog',
			'posts_per_page' => $lista
		);
		$content = [];
		$query = new WP_Query( $args );
		while ( $query->have_posts() ) : $query->the_post();
			$content = [
				"imagem" => get_field('type-blog-imagem', $post->ID),
				"titulo" => [
					"ptbr" => get_field('type-blog-ptbr-titulo', $post->ID),
					"enus" => get_field('type-blog-enus-titulo', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('type-blog-ptbr-conteudo', $post->ID),
					"enus" => get_field('type-blog-enus-conteudo', $post->ID)
				]
			];

		?>
		<div class="item">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-1">
						<img class="img-response" src="<?php echo $content['imagem']['sizes']['blog'];?>"/>
					</div>
					<div class="col-md-4">
						<div class="contenttext">
							<h1><?php echo $content['titulo'][$lang];?></h1>
              				<time datetime="<?php echo get_the_date('Y/m/d');?>"><?php echo dateLang($lang); ?></time>
							<p><?php echo reduzirTexto($content['conteudo'][$lang], 20); ?></p>
              				<a href="<?php the_permalink();?>" class="leiamais">Leia mais +</a>
            			</div>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile;wp_reset_postdata();?>


		<div class="paginacao">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="paginacao-items"></div>
					</div>
					<div class="col-md-12 h--txt-center">
						<button class="button--base-big button--gd-hs1 paginacao--botao" data-lista="<?=$lista?>" data-lang="<?=$lang?>" data-regra="Blog" data-requisita="blog" data-requisitapagina="2">ver mais</button>
					</div>
				</div>
			</div>
		</div>


	</div>
</section>

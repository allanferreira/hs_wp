<?php
require_once "../wp-load.php";
require_once "../model/Paginacao.php";


class Depoimentos extends Paginacao {

    public function monta($paginaRequisitada, $lista, $lang) {

        $content = [];
		$query = new WP_Query([
            'post_type' => $this->requisita,
            'posts_per_page' => $lista,
            'paged' => $paginaRequisitada
        ]);

		if ( $query->have_posts() ):
    		while ( $query->have_posts() ) : $query->the_post();
                $content = [
                    "imagem" => get_field('type-depoimentos-imagem', $post->ID),
                    "idade" => [
                        "ptbr" => get_field('type-depoimentos-ptbr-idade', $post->ID),
                        "enus" => get_field('type-depoimentos-enus-idade', $post->ID)
                    ],
                    "local" => [
                        "ptbr" => get_field('type-depoimentos-ptbr-local', $post->ID),
                        "enus" => get_field('type-depoimentos-enus-local', $post->ID)
                    ],
                    "conteudo" => [
                        "ptbr" => get_field('type-depoimentos-ptbr-conteudo', $post->ID),
                        "enus" => get_field('type-depoimentos-enus-conteudo', $post->ID)
                    ]
                ];
                ?>

                    <div class="item">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <img class="img-response" src="<?php echo $content['imagem'];?>"/>
                                </div>
                                <div class="col-md-8">
                                    <div class="contenttext">
                                        <div class="contenttext-title">
                                            <h1>
                                                <?php the_title();?>
                                            </h1>
                                            <span class="contenttext-title-sub"><?php echo $content['local'][$lang];?></span>
                                            <span class="bar"></span>
                                        </div>
                                        <?php echo $content['conteudo'][$lang];?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
            endwhile;
        endif;
        wp_reset_postdata();

    }

}

<section class="section--text section--ha onde-voce-quer-estudarSection">
	<div class="container">
		<div class="row">
			<div class="col-md-12 center">
				<div class="title">
					<h1><?php echo $state['statics']['titulo'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
          <div class="carouselList">
            <div class="icon iconprev">
              <img src="<?php echo bloginfo('template_url');?>/img/carousel/left.png">
            </div>
            <div class="icon iconnext">
              <img src="<?php echo bloginfo('template_url');?>/img/carousel/right.png">
            </div>
            <div class="owl-carousel owl-theme">
              <?php
          		$args = array(
          			'post_type' => 'cidades',
          			'posts_per_page' => -1,
                'tax_query' => array( array(
        					'taxonomy' => 'cidades-categories',
        					'field' => 'slug',
        					'terms' => 'he'
                ))
          		);
          		$content = [];
          		$query = new WP_Query( $args );
          		while ( $query->have_posts() ) : $query->the_post();
          			$content = [
          				"imagem" => get_field('type-cidades-imagem', $post->ID),
          				"titulo" => [
          					"ptbr" => get_field('type-cidades-ptbr-titulo', $post->ID),
          					"enus" => get_field('type-cidades-enus-titulo', $post->ID)
          				],
          				"conteudo" => [
          					"ptbr" => get_field('type-cidades-ptbr-conteudo', $post->ID),
          					"enus" => get_field('type-cidades-enus-conteudo', $post->ID)
          				]
          			];
          		?>
              <div class="item">
                <div class="hover">
                  <h1><?php echo $content['titulo'][$lang];?></h1>
                  <p><?php echo reduzirTexto($content['conteudo'][$lang], 13, '...'); ?></p>
                  <div class="button-wrap">
                    <a href="<?php the_permalink();?>">
                      <button class="button--base"><?php echo $state['statics']['saibamais'][$lang];?></button>
                    </a>
                  </div>
                </div>
                <img src="<?php echo $content['imagem']['sizes']['carouselList'];?>">
              </div>
              <?php endwhile;wp_reset_postdata();?>
            </div>
          </div>
          <a href="<?php echo home_url();?>/he/cidades">
            <button class="button--base-big button--gd-ha1"><?php echo $state['statics']['maiscidades'][$lang];?></button>
          </a>
				</div>
			</div>
		</div>
	</div>
</section>

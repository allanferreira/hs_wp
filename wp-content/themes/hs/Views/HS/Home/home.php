<div class="homeView">
	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('home')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"galeria" => [
				"ptbr" => get_field('hs-home-ptbr-galeria', $post->ID),
				"enus" => get_field('hs-home-enus-galeria', $post->ID),
			],
			"titulo" => [
				"ptbr" => get_field('hs-home-ptbr-titulo', $post->ID),
				"enus" => get_field('hs-home-enus-titulo', $post->ID)
			],
			"conteudo" => [
				"ptbr" => get_field('hs-home-ptbr-conteudo', $post->ID),
				"enus" => get_field('hs-home-enus-conteudo', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>
	<?php
	$component = new Component;
	$component->create([
		"banner_box" => true,
		"content" => $content['galeria'],
		"banner-destino" => 'hs',
		"saibamais" => [
			"ptbr" => "Saiba mais",
			"enus" => "More"
		]
	], 'banner', LANGUAGE);
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => [
			"servicosTitle" => $content['titulo'],
			"servicosContent" => $content['conteudo'],
			"saibamais" => [
				"ptbr" => "Saiba mais",
				"enus" => "More"
			],
			"servicosCurso" => [
				"ptbr" => "Curso",
				"enen" => "Curso",
				"enau" => "Curso",
				"espe" => "Curso",
				"esco" => "Curso"
			],
			"servicosAcomodacao" => [
				"ptbr" => "Acomodação",
				"enen" => "Acomodação",
				"enau" => "Acomodação",
				"espe" => "Acomodação",
				"esco" => "Acomodação"
			],
			"servicosVisto" => [
				"ptbr" => "Visto",
				"enen" => "Visto",
				"enau" => "Visto",
				"espe" => "Visto",
				"esco" => "Visto"
			],
			"servicosPassagemAerea" => [
				"ptbr" => "Passagem\nAérea",
				"enen" => "Passagem\nAérea",
				"enau" => "Passagem\nAérea",
				"espe" => "Passagem\nAérea",
				"esco" => "Passagem\nAérea"
			],
			"servicosAssistenciaViagem" => [
				"ptbr" => "Assistência\nViagem",
				"enen" => "Assistência\nViagem",
				"enau" => "Assistência\nViagem",
				"espe" => "Assistência\nViagem",
				"esco" => "Assistência\nViagem"
			],
			"servicosCartaoMultiMoeda" => [
				"ptbr" => "Cartão\nMulti-moedas",
				"enen" => "Cartão\nMulti-moedas",
				"espe" => "Cartão\nMulti-moedas",
				"esco" => "Cartão\nMulti-moedas"
			]
		]
	], 'servicos', LANGUAGE, 'Views/HS/Home/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => [
			"depoimentosTitle" => [
				"ptbr" => get_field('hs-home-ptbr-depoimentos-titulo', $post->ID),
				"enus" => get_field('hs-home-enus-depoimentos-titulo', $post->ID)
			],
			"depoimentosConteudo" => [
				"ptbr" => get_field('hs-home-ptbr-depoimentos-conteudo', $post->ID),
				"enus" => get_field('hs-home-enus-depoimentos-conteudo', $post->ID)
			],
			"leiamais" => [
				"ptbr" => "Leia mais",
				"enus" => "Read more"
			]
		]
	], 'depoimentos', LANGUAGE, 'Views/HS/Home/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => [
			"blogTitle" => [
				"ptbr" => "Blog",
				"enus" => "Blog"
			],
			"leiamais" => [
				"ptbr" => "Leia mais",
				"enus" => "Read more"
			]
		]
	], 'blog', LANGUAGE, 'Views/HS/Home/Parts');
	?>
</div>

<?php
function logo_in_admin_url() {
    return get_bloginfo('url');
}

function logo_in_admin_url_title() {
    return ' ';
}

function logo_in_admin() {
	?><link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo bloginfo('template_url');?>/admin/css/login.css" type="text/css" media="all" /><?php
}

function irParaLinkSeguro(){
	$local = in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ));
	if ($_SERVER["SERVER_PORT"] != 443 and !$local) {
	    header("Location: https://hellostudy.com.br/hs_wp/wp-admin");
	    exit();
	}
}
<section class="promocoespassagenshaView">
  <div class="nav--menu-height-single"></div>
  <?php
  $content = [];
  $cats = [get_cat_ID('hu'), get_cat_ID('promocoespassagens')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "hu-promocoespassagens-texto" => [
          "ptbr" => get_field('hu-promocoespassagens-ptbr-texto', $post->ID),
          "enus" => get_field('hu-promocoespassagens-enus-texto', $post->ID)
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>
  <?php
  $component = new Component;
  $component->create([
    "texto" => $content["hu-promocoespassagens-texto"],
    "static" => [
      "titulo" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ],
      "passagem" => [
        "ptbr" => "Passagem",
        "enus" => "Passagem"
      ]
    ]
], 'item', LANGUAGE, 'Views/HU/Promocoespassagens/Parts');
	?>
</section>

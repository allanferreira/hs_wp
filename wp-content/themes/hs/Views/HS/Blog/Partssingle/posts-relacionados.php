<?php
$args = array(
  'post_type' => 'blog',
  'posts_per_page' => 4,
  'post__not_in' => $state['statics']['current_id']
);
$query = new WP_Query( $args );
if($query->have_posts()):
?>
  <section class="section--text hs-single-posts-relacionados">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
  				<div class="title">
  					<h1><?php echo $state['statics']['postsrelacionados'][$lang];?></h1>
  				</div>
        </div>
      </div>
      <div class="row posts">
        <?php
        while ( $query->have_posts() ) : $query->the_post();
          $content = [
            "imagem" => get_field('type-blog-imagem', $post->ID),
            "titulo" => [
              "ptbr" => get_field('type-blog-ptbr-titulo', $post->ID),
              "enus" => get_field('type-blog-enus-titulo', $post->ID)
            ],
            "conteudo" => [
              "ptbr" => get_field('type-blog-ptbr-conteudo', $post->ID),
              "enus" => get_field('type-blog-enus-conteudo', $post->ID)
            ]
          ];
        ?>
          <div class="col-md-3 post">
            <div class="post__content">
              <div class="post__content__image">
                <div class="post__content__image__bg"></div>
                <img src="<?php echo $content["imagem"]["sizes"]["blogportable"];?>" />
              </div>
              <h1><?php echo $content['titulo'][$lang];?></h1>
              <time datetime="<?php echo get_the_date('Y/m/d');?>"><?php echo dateLang($lang); ?></time>
              <p><?php echo reduzirTexto($content['conteudo'][$lang], 20); ?></p>
              <a href="<?php the_permalink();?>">
                <?php echo $state['statics']['leiamais'][$lang];?> +
              </a>
            </div>
          </div>
        <?php endwhile;?>
  		</div>
  	</div>
  </section>
<?php endif;?>

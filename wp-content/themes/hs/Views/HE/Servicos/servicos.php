<section class="servicoshaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$content = [];
	$cats = [get_cat_ID('he'), get_cat_ID('servicos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"page" => [
  			"titulo" => [
  				"ptbr" => get_field('he-servicos-ptbr-titulo', $post->ID),
  				"enus" => get_field('he-servicos-enus-titulo', $post->ID)
  			],
  			"conteudo" => [
  				"ptbr" => get_field('he-servicos-ptbr-conteudo', $post->ID),
  				"enus" => get_field('he-servicos-enus-conteudo', $post->ID)
  			]
  		],
      "items" => [
        "ptbr" => get_field('he-servicos-ptbr-item', $post->ID),
        "enus" => get_field('he-servicos-enus-item', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['page'],
    "class" => "ha-servicos"
  ], 'sectionTextSimple', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content['items']
	], 'items', LANGUAGE, 'Views/HE/Servicos/Parts');
	?>


</section>

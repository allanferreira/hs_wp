<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'magorag_hs');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dPjkJQ,S;X?CH<djz/XiR[I]ucR/dOyB`li#833-b(B7^;&D3RzBXTN *E_[X;[2');
define('SECURE_AUTH_KEY',  '}CzGrBBG[Nk{STex#m#h`Y+si:o2eS2rZ7EE~tv2zd=VQ+mG%8L>=5c3}dsS[/O.');
define('LOGGED_IN_KEY',    'Sy9d787PR$V`HU2KZ06?N.iXX(L)E&KODJ<LOH)yC/}F{0p%UhMQ,O_l[G4abpUz');
define('NONCE_KEY',        'i(V8*>D9q CMmZVQi2t]pAv<=f*q(:1Mt_y>].b2E9O|Q`*t@ya3!;)U^PHBs}Dh');
define('AUTH_SALT',        '7p/IEnD}gI{fh;]6`btO?P+6n+( 40QD0?wY=TR}nRvIg8hA8<~$$o2H>u:-MMs]');
define('SECURE_AUTH_SALT', '6t|3 )C=L[g;2|Fo}tU27h+r9PrarYY./i_13o<YOa_,?j(8]{TI=3HUptoFeY%b');
define('LOGGED_IN_SALT',   'lu!oIHp%HSMGZLacE34Tos%&c5vhicyh?vNoDZ(OL|hPgdnox*q{nj?P#v0GVt{(');
define('NONCE_SALT',       'q{COPzl[+v:Oy2$4KNA|msYf[_.5zL/TgjrTNgv}>WKtPRni`9=>7Hyz/zvmTL:?');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'magorag_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

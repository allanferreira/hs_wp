<div class="panel-group" id="trabalho" role="tablist" aria-multiselectable="true">
  <?php
  $count = 0;
  foreach ($state['content'][$lang] as $key => $item):
  $count++;
  $titulo = 'ha-trabalho-'.$lang.'-lista-titulo';
  $conteudo = 'ha-trabalho-'.$lang.'-lista-conteudo';
  ?>
  <div class="panel panel-default collapsed">
    <div class="panel-heading" role="tab" id="heading<?php echo $count;?>"  data-toggle="collapse" href="#<?php echo $titulo.$count;?>">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="panel-title">
              <a href="#">
                <?php echo $item[$titulo];?>
                <div class="arrow"></div>
              </a>
            </h2>
          </div>
        </div>
      </div>
    </div>
    <div id="<?php echo $titulo.$count;?>" class="collapse">
      <div class="panel-body">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="padding-wrapper">
                <span></span>
                <div class="wrapper-content">
                  <?php echo $item[$conteudo];?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="line"></div>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach;?>
</div>

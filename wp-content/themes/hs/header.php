<?php
$local = in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ));
if ($_SERVER["SERVER_PORT"] != 443 and !$local) {
    header("Location: https://hellostudy.com.br/hs_wp/");
    exit();
}

if(isset($_GET['lg'])){
	define("LANGUAGE", $_GET['lg']);
} else {
	define("LANGUAGE", "ptbr");
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo bloginfo('template_url');?>/dist/css/app.css">

		<?php wp_head(); ?>
	</head>
	<body>
		<div class="visible-md visible-lg">
			<?php
			$component = new Component;
			$component->create([
				"content" => [
					"orcamento" => [
						"ptbr" => "Orçamento",
						"enus" => "Bugget",
						"eses" => "Presupuesto"
					]
				]
			], 'navMenu', LANGUAGE);
			?>
		</div>
		<div class="visible-xs visible-sm">
			<?php
			$component = new Component;
			$component->create([
				"tela" => "hs",
				"content" => [
					"orcamento" => [
						"ptbr" => "Orçamento",
						"enus" => "Bugget",
						"eses" => "Presupuesto"
					]
				]
			], 'navMenuMobile', LANGUAGE);
			?>
		</div>

<section class="section--text section--ha pensando-em-ir-para-a-australiaSection">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
        <iframe src="<?php echo $state['content']['link'][$lang];?>" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-md-6">
        <div class="info">
          <div class="title">
            <h1><?php echo $state['content']['titulo'][$lang];?></h1>
            <span></span>
          </div>
          <p><?php echo $state['content']['conteudo'][$lang];?></p>
        </div>
      </div>
    </div>
  </div>
</section>

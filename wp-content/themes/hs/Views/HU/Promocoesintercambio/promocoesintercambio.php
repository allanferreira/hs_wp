<section class="promocoesinterhaView">
  <div class="nav--menu-height-single"></div>
  <?php
  $content = [];
  $cats = [get_cat_ID('hu'), get_cat_ID('promocoesintercambio')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "hu-promocoesintercambio-texto" => [
          "ptbr" => get_field('hu-promocoesintercambio-ptbr-texto', $post->ID),
          "enus" => get_field('hu-promocoesintercambio-enus-texto', $post->ID)
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>
  <?php
  $component = new Component;
  $component->create([
    "texto" => $content["hu-promocoesintercambio-texto"],
    "static" => [
      "titulo" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ]
    ]
], 'item', LANGUAGE, 'Views/HU/Promocoesintercambio/Parts');
	?>
</section>

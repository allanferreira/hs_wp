const NavMenuMobile = {
  inicia: () => {

      NavMenuMobile.checaDevice()

  },

  executa: () => {

      NavMenuMobile.aba()
      NavMenuMobile.dropdown()
      NavMenuMobile.open()
      NavMenuMobile.close()

  },

  checaDevice: () => {

    let w = $(window).width()

    if(w < 768)
      NavMenuMobile.executa()

  },

  aba: () => {

    $('.nav-mobile__destinos ul li').click(function(){
      $('.nav-mobile__destinos ul li, .nav-mobile .menu, .nav-mobile__menu > li').removeClass('active')

      var menuEscolhido = $(this).data('aba')
      $('.menu'+menuEscolhido).addClass('active')
      $(this).addClass('active');
    })

  },

  dropdown: () => {

    $('.nav-mobile__menu > li').click(function(){
      let temDropdown = $(this).find('ul')
      if(temDropdown){
        $(this).toggleClass('active')
      }
    })

  },

  open: () => {

    $('.nav-mobile-open').click(function(){
      $('.nav-mobile').addClass('active')
      $('.nav-mobile-open .icon--menu').removeClass('open')
    })

  },

  close: () => {

    $('.nav-mobile__close').click(function(){
      $('.nav-mobile').removeClass('active')
    })

  }
}

const menuMob = NavMenuMobile
menuMob.inicia()

$(window).resize(function(){
  menuMob.inicia()
})

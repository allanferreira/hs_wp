const gulp   = require('gulp')
	, stylus = require('gulp-stylus')
	, autoprefixer = require('gulp-autoprefixer')
	, plumber      = require('gulp-plumber')
	, browserify = require('gulp-browserify')

const _DIR_  = './wp-content/themes/hs'

gulp.task('stylus', () => {
	return gulp.src(_DIR_ + '/src/css/app.styl')
		.pipe(plumber())
		.pipe(stylus({
			'include css': true,
		  	compress: true
		}))
		.pipe(plumber.stop())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
		.pipe(gulp.dest(_DIR_ + '/dist/css'))
})

 

gulp.task('js', function() {
    gulp.src(_DIR_ + '/src/js/app.js')
		.pipe(plumber())
        .pipe(browserify({
			insertGlobals : true,
			debug : !gulp.env.production
        }))
		.pipe(plumber.stop())
        .pipe(gulp.dest(_DIR_ + '/dist/js'))
})

gulp.task('watch', () => {
    gulp.watch(_DIR_ + '/src/css/**/*.styl', ['stylus'])
    gulp.watch(_DIR_ + '/src/js/**/*.js', ['js'])
    gulp.watch(_DIR_ + '/src/js/components/*.js', ['js'])
})

gulp.task('build', ['stylus','js'])
gulp.task('default', ['build'])

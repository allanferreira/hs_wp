<div class="section--text ha-promocoesintercambio">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title">
          <h1><?=$state['static']['titulo'][$lang]?></h1>
          <span></span>
        </div>
      </div>
    </div>
    <div class="row items">
      <?php
      $args = array(
        'post_type' => 'promocoes-inter',
        'posts_per_page' => -1,
        'tax_query' => array( array(
          'taxonomy' => 'promocoes-inter-categories',
          'field' => 'slug',
          'terms' => 'he'
        ))
      );
      $content = [];
      $count = 0;
      $query = new WP_Query( $args );
      while ( $query->have_posts() ) : $query->the_post();
        $content = [
          "imagem" => get_field('type-promocoesint-image', $post->ID),
          "titulo" => [
            "ptbr" => get_field('type-promocoesint-ptbr-titulo', $post->ID),
            "enus" => get_field('type-promocoesint-enus-titulo', $post->ID)
          ],
          "cidade" => [
            "ptbr" => get_field('type-promocoesint-ptbr-cidade', $post->ID),
            "enus" => get_field('type-promocoesint-enus-cidade', $post->ID)
          ],
          "conteudo" => [
            "ptbr" => get_field('type-promocoesint-ptbr-conteudo', $post->ID),
            "enus" => get_field('type-promocoesint-enus-conteudo', $post->ID)
          ],
          "valor" => [
            "ptbr" => get_field('type-promocoesint-ptbr-valor', $post->ID),
            "enus" => get_field('type-promocoesint-enus-valor', $post->ID)
          ],
          "parcelas" => [
            "ptbr" => get_field('type-promocoesint-ptbr-parcelas', $post->ID),
            "enus" => get_field('type-promocoesint-enus-parcelas', $post->ID)
          ],
          "fixas" => [
            "ptbr" => get_field('type-promocoesint-ptbr-fixas', $post->ID),
            "enus" => get_field('type-promocoesint-enus-fixas', $post->ID)
          ],
          "detalhe" => [
            "ptbr" => get_field('type-promocoesint-ptbr-detalhe', $post->ID),
            "enus" => get_field('type-promocoesint-enus-detalhe', $post->ID)
          ],
          "linkform" => [
            "ptbr" => get_field('type-promocoesint-ptbr-linkform', $post->ID),
            "enus" => get_field('type-promocoesint-enus-linkform', $post->ID)
          ]
        ];
        $count = $count == 2 ? 0 : $count;
        $lado = $count == 0 ? 'left' : 'right';
        $line = $count == 1 ? 'line' : '';
      ?>
        <div class="col-md-6">
          <div class="item <?=$lado?>">
            <img class="image" src="<?php echo $content['imagem']['sizes']['promocoes'];?>">
            <h1>
              <?php echo $content['titulo'][$lang];?>
              <?php
              $cidade = $content['cidade'][$lang];
              if ($cidade != "") {
                echo "(" . $cidade->post_title . ")"; 
              }
              ?>
            </h1>
            <div class="content">
              <?php echo $content['conteudo'][$lang];?>
            </div>
            <div class="valor">
              <?php echo valorPromocoes($content['valor'][$lang], $lang);?>
            </div>
            <div class="parcelas">
              <strong><?php echo $content['parcelas'][$lang];?></strong>
              <strong><?php echo $content['fixas'][$lang];?></strong>
            </div>
            <div class="detalhe">
              <?php echo $content['detalhe'][$lang];?>
            </div>

            <a href="<?php echo $content['linkform'][$lang];?>">
              <button class="button--base-big button--gd-hs1"><?php echo $state['static']['estouinteressado'][$lang];?></button>
            </a>
          </div>
        </div>
        <div class="<?=$line?>"></div>
      <?php $count++; endwhile; wp_reset_postdata();?>
            <?php if($count == 1){ ?>
      <div class="line"></div>
      <?php } ?>
    </div>
    
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="texto">
          <?php echo $state['texto'][$lang];?>
        </div>
      </div>
    </div>
  </div>
</div>

$(window).scroll(function () {
	const nav = $('.nav--menu__bar')
	if ($(this).scrollTop() > 300) {
		nav.addClass('scroll')
	} else {
		nav.removeClass('scroll')
	}
})
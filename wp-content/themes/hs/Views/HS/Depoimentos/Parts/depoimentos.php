<section class="section--text hs-depoimentos-depoimentos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
	</div>

	<div class="content">

		<?php
		$lista = 5;
		$args = array(
			'post_type' => 'depoimentos',
			'posts_per_page' => $lista
		);
		$content = [];
		$query = new WP_Query( $args );
		while ( $query->have_posts() ) : $query->the_post();
			$content = [
				"imagem" => get_field('type-depoimentos-imagem', $post->ID),
				"idade" => [
					"ptbr" => get_field('type-depoimentos-ptbr-idade', $post->ID),
					"enus" => get_field('type-depoimentos-enus-idade', $post->ID)
				],
				"local" => [
					"ptbr" => get_field('type-depoimentos-ptbr-local', $post->ID),
					"enus" => get_field('type-depoimentos-enus-local', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('type-depoimentos-ptbr-conteudo', $post->ID),
					"enus" => get_field('type-depoimentos-enus-conteudo', $post->ID)
				]
			];
		?>
		<div class="item">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<img class="img-response" src="<?php echo $content['imagem'];?>"/>
					</div>
					<div class="col-md-8">
						<div class="contenttext">
							<div class="contenttext-title">
								<h1>
									<?php the_title();?>
								</h1>
								<span class="contenttext-title-sub"><?php echo $content['local'][$lang];?></span>
								<span class="bar"></span>
							</div>
							<?php echo $content['conteudo'][$lang];?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endwhile;wp_reset_postdata();?>


		<div class="paginacao">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="paginacao-items"></div>
					</div>
					<div class="col-md-12 h--txt-center">
						<button class="button--base-big button--gd-hs1 paginacao--botao" data-lista="<?=$lista?>" data-lang="<?=$lang?>" data-regra="Depoimentos" data-requisita="depoimentos" data-requisitapagina="2">ver mais</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

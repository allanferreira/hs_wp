<div class="aaustraliaView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('ha-aaustralia-ptbr-titulo', $post->ID),
        "enus" => get_field('ha-aaustralia-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('ha-aaustralia-ptbr-conteudo', $post->ID),
        "enus" => get_field('ha-aaustralia-enus-conteudo', $post->ID)
      ]
    ],
    "imgdestaque" => get_field('ha-aaustralia-imagem-destaque', $post->ID),
    "imgdetalhe" => get_field('ha-aaustralia-imagem-detalhe', $post->ID),
    "detalhes" => [
      "ptbr" => get_field('ha-aaustralia-ptbr-detalhes', $post->ID),
      "enus" => get_field('ha-aaustralia-enus-detalhes', $post->ID)
    ],
    "porqueestudar" => [
      "titulo" => [
        "ptbr" => get_field('ha-aaustralia-ptbr-porqueestudartitulo', $post->ID),
        "enus" => get_field('ha-aaustralia-enus-porqueestudartitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('ha-aaustralia-ptbr-porqueestudarconteudo', $post->ID),
        "enus" => get_field('ha-aaustralia-enus-porqueestudarconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
	$component = new Component;
	$component->create([
    "image" => $content['imgdestaque']
  ], 'banner', LANGUAGE, 'Views/HA/Aaustralia/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "ha-aaustralia-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
    "content" => $content['detalhes'],
    "image" => $content['imgdetalhe']
  ], 'detalhes', LANGUAGE, 'Views/HA/Aaustralia/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['porqueestudar'],
    "class" => "ha-aaustralia-porqueestudar"
  ], 'sectionText', LANGUAGE);
  ?>
</div>

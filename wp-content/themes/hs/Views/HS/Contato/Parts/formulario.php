<section class="section--text hs-contato-formulario">
  <div class="container">
    <div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['hs-contato-contato']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
    <div class="row">
			<div class="col-md-8 col-md-offset-2">
        <div class="content">
            <?php echo $state['hs-contato-contato']['conteudo'][$lang];?>
            <iframe class="iframeContato" src="<?=$state['statics']['iframe'][$lang]?>"></iframe>
        </div>
        <!--div class="formulario" id="contato">
          <form @submit.prevent>
            <div class="row">
              <div class="col-md-12">
                <label><?php echo $state['statics']['assunto'][$lang];?></label>
                <div class="select" :class="{'input': true, 'is-danger': errors.has('assunto') }">
                  <select v-validate="'required|not_in:x'" name="assunto" v-model="assunto">
                    <option value=""><?php echo $state['statics']['escolhaumassunto'][$lang];?></option>
                    <?php
                    $items = $state['hs-contato-formulario'][$lang];
                    foreach ($items as $key => $item){
                      echo '<option value="'.$item['hs-contato-formulario-'.$lang.'-email'].'">'.$item['hs-contato-formulario-'.$lang.'-assunto'].'</option>';
                    }
                    ?>
                  </select>
                  <div class="h--clear"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label><?php echo $state['statics']['nome'][$lang];?></label>
                <input v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('nome') }" type="text" name="nome" v-model="nome"/>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label><?php echo $state['statics']['email'][$lang];?></label>
                <input v-validate="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="text" name="email" v-model="email"/>
              </div>
              <div class="col-md-6">
                <label><?php echo $state['statics']['telefone'][$lang];?></label>
                <input v-validate="'required|numeric|min:8|max:11'" :class="{'input': true, 'is-danger': errors.has('telefone') }" type="text" name="telefone" v-model="telefone"/>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label><?php echo $state['statics']['mensagem'][$lang];?></label>
                <textarea name="mensagem" v-model="mensagem" v-validate="'required'" :class="{'input': true, 'is-danger': errors.has('mensagem') }"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                {{msg}}
                <input @click="validate" class="button--base-big button--gd-hs1" type="submit" value="<?php echo $state['statics']['enviar'][$lang];?>"/>
              </div>
            </div>
          </form>
        </div-->
      </div>
    </div>
  </div>
</section>

<div class="aaustraliaView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('hu-osestadosunidos-ptbr-titulo', $post->ID),
        "enus" => get_field('hu-osestadosunidos-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hu-osestadosunidos-ptbr-conteudo', $post->ID),
        "enus" => get_field('hu-osestadosunidos-enus-conteudo', $post->ID)
      ]
    ],
    "imgdestaque" => get_field('hu-osestadosunidos-imagem-destaque', $post->ID),
    "imgdetalhe" => get_field('hu-osestadosunidos-imagem-detalhe', $post->ID),
    "detalhes" => [
      "ptbr" => get_field('hu-osestadosunidos-ptbr-detalhes', $post->ID),
      "enus" => get_field('hu-osestadosunidos-enus-detalhes', $post->ID)
    ],
    "porqueestudar" => [
      "titulo" => [
        "ptbr" => get_field('hu-osestadosunidos-ptbr-porqueestudartitulo', $post->ID),
        "enus" => get_field('hu-osestadosunidos-enus-porqueestudartitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('hu-osestadosunidos-ptbr-porqueestudarconteudo', $post->ID),
        "enus" => get_field('hu-osestadosunidos-enus-porqueestudarconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
	$component = new Component;
	$component->create([
    "image" => $content['imgdestaque']
], 'banner', LANGUAGE, 'Views/HU/Osestadosunidos/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "ha-aaustralia-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
    "content" => $content['detalhes'],
    "image" => $content['imgdetalhe']
], 'detalhes', LANGUAGE, 'Views/HU/Osestadosunidos/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['porqueestudar'],
    "class" => "ha-aaustralia-porqueestudar"
  ], 'sectionText', LANGUAGE);
  ?>
</div>

<?php
add_theme_support('post-thumbnails');
add_action( 'init', function(){ register_taxonomy_for_object_type( 'category', 'page' );});
add_action( 'show_admin_bar', function(){ return false;});
add_action( 'login_enqueue_scripts', 'irParaLinkSeguro' );
add_action( 'login_enqueue_scripts', 'logo_in_admin' );
add_filter( 'login_headerurl', 'logo_in_admin_url' );
add_filter( 'login_headertitle', 'logo_in_admin_url_title' );

new Type('Depoimentos','dashicons-edit');
new Type('Blog','dashicons-edit');
new Type('Eventos','dashicons-edit');
new Type('Cidades','dashicons-edit');
new Type('Promoções Inter','dashicons-edit');
new Type('Promoções Passagens','dashicons-edit');
//new Type('Destinos','dashicons-edit');

new ThumbSize('blogportable', 292, 205, true);
new ThumbSize('blog', 670, 272, true);
new ThumbSize('blogDestaque', 1000, 530, true);
new ThumbSize('eventos', 474, 304, true);
new ThumbSize('equipe', 293, 265, true);
new ThumbSize('marcas', 133, 110, false);
new ThumbSize('carouselList', 285, 285, true);
new ThumbSize('promocoes', 444, 336, true);
new ThumbSize('promocoesBg', 1600, 550, true);
new ThumbSize('wide', 1600, 589, true);
new ThumbSize('cidades', 285, 285, true);
new ThumbSize('cidadeDetalhe', 800, 530, true);
new ThumbSize('bannerhome', 1600, 1000, true);
<section class="section--text hs-single-artigo">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<time datetime="<?php echo get_the_date('Y/m/d');?>"><?php echo dateLang($lang); ?></time>
				</div>
				<div class="content">
					<img class="destaque" src="<?php echo $state['content']['imagem']['sizes']['blogDestaque'];?>" alt="<?php echo $state['content']['imagem']['description'];?>"/>
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
		</div>
	</div>
</section>

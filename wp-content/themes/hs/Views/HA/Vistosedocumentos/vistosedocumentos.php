<section class="vistosedocumentoshaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$content = [];
	$cats = [get_cat_ID('ha'), get_cat_ID('vistosedocumentos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"vistos" => [
  			"conteudo" => [
    			"titulo" => [
    				"ptbr" => get_field('ha-vistos-ptbr-titulo', $post->ID),
    				"enus" => get_field('ha-vistos-enus-titulo', $post->ID)
    			],
    			"conteudo" => [
    				"ptbr" => get_field('ha-vistos-ptbr-conteudo', $post->ID),
    				"enus" => get_field('ha-vistos-enus-conteudo', $post->ID)
    			]
        ],
        "lista" => [
          "ptbr" => get_field('ha-vistos-ptbr-lista', $post->ID),
          "enus" => get_field('ha-vistos-enus-lista', $post->ID)
        ]
      ],
			"documentos" => [
  			"conteudo" => [
    			"titulo" => [
    				"ptbr" => get_field('ha-documentos-ptbr-titulo', $post->ID),
    				"enus" => get_field('ha-documentos-enus-titulo', $post->ID)
    			],
    			"conteudo" => [
    				"ptbr" => get_field('ha-documentos-ptbr-conteudo', $post->ID),
    				"enus" => get_field('ha-documentos-enus-conteudo', $post->ID)
    			]
        ],
        "lista" => [
          "ptbr" => get_field('ha-documentos-ptbr-lista', $post->ID),
          "enus" => get_field('ha-documentos-enus-lista', $post->ID)
        ]
      ]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['vistos']['conteudo'],
    "class" => "ha-vistos"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content['vistos']['lista']
	], 'vistos', LANGUAGE, 'Views/HA/Vistosedocumentos/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['documentos']['conteudo'],
    "class" => "ha-documentos"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content['documentos']['lista']
	], 'documentos', LANGUAGE, 'Views/HA/Vistosedocumentos/Parts');
	?>
</section>

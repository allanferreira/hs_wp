<?php
require_once "../wp-load.php";
require_once "../model/Paginacao.php";


class Blog extends Paginacao {

    public function monta($paginaRequisitada, $lista, $lang) {

        $content = [];
		$query = new WP_Query([
            'post_type' => $this->requisita,
            'posts_per_page' => $lista,
            'paged' => $paginaRequisitada
        ]);

		if ( $query->have_posts() ):
    		while ( $query->have_posts() ) : $query->the_post();
                $content = [
                    "imagem" => get_field('type-blog-imagem', $post->ID),
                    "titulo" => [
                        "ptbr" => get_field('type-blog-ptbr-titulo', $post->ID),
                        "enus" => get_field('type-blog-enus-titulo', $post->ID)
                    ],
                    "conteudo" => [
                        "ptbr" => get_field('type-blog-ptbr-conteudo', $post->ID),
                        "enus" => get_field('type-blog-enus-conteudo', $post->ID)
                    ]
                ];
                ?>

                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-1">
                                <img class="img-response" src="<?=$content['imagem']['sizes']['blog']?>"/>
                            </div>
                            <div class="col-md-4">
                                <div class="contenttext">
                                    <h1><?=$content['titulo'][$lang];?></h1>
                                    <time datetime="<?=get_the_date('Y/m/d')?>"><?=dateLang($lang)?></time>
                                    <p><?=reduzirTexto($content['conteudo'][$lang], 20)?></p>
                                    <a href="<?php the_permalink() ?>" class="leiamais">Leia mais +</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            endwhile;
        endif;
        wp_reset_postdata();

    }

}

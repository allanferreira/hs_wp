<div class="parceirosView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('ha'), get_cat_ID('parceiros')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"ha-parceiros-titulo" => [
  			"parceiros" => [
  				"ptbr" => get_field('ha-parceiros-parceiros-ptbr-titulo', $post->ID),
  				"enus" => get_field('ha-parceiros-parceiros-enus-titulo', $post->ID)
  			],
  			"escolas" => [
  				"ptbr" => get_field('ha-parceiros-escolas-ptbr-titulo', $post->ID),
  				"enus" => get_field('ha-parceiros-escolas-enus-titulo', $post->ID)
  			]
  		],
      "ha-parceiros-marcas" => [
        "parceiros" => get_field('ha-parceiros-parceiros-marcas', $post->ID),
  			"escolas" => get_field('ha-parceiros-parceiros-escolas', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'parceiros', LANGUAGE, 'Views/HA/Parceiros/Parts');
  ?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'escolas', LANGUAGE, 'Views/HA/Parceiros/Parts');
	?>
</div>

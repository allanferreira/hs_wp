$('.banner__box__item')
    .hover(function(){
    	$(this).find('button')
    		.addClass('animated flipInX')
    },function(){
    	$(this).find('button')
    		.removeClass('animated flipInX')
    })

let banner = $('.banner__slider')
banner.owlCarousel({
    loop:true,
    margin:0,
    nav:true,
	items:1,
	autoplayHoverPause:false,
	autoplay: true,
    autoPlaySpeed: 1000,
    lazyLoad : true,
    animateOut: 'fadeOut',
    autoPlayTimeout: 1000,
    navText: ['',''],
    onInitialized: calculaAlturaBanner()
})

function calculaAlturaBanner(){

    let alturaTela = $(window).height()
    let alturaMenu = $('.nav--menu').height()
    
    let estouNaHome = $('body').find('.homeView').length != 0
    let alturaBannerBox = estouNaHome ? $('.banner__box').height() : 0

    let alturaNova = alturaTela - (alturaMenu + alturaBannerBox)

    let bannerText = $('.banner__slider__item__content')
    bannerText.css({
        height: alturaNova,
        marginTop: alturaMenu
    })

    let metadeTela = (alturaNova/2)+alturaMenu
    setTimeout(() => calculaDetalhesBanner(metadeTela),100)
    
}

function calculaDetalhesBanner(metadeTela){
    
    let pontos = $('.owl-dots')
        , setas = $('.owl-prev,.owl-next')
        , bannerBoxMobile = $('.banner.home .mobileBox')


    setas.css({top: metadeTela})
    pontos.css({
        top: metadeTela,
        marginTop: -(pontos.height()/2)
    })

    bannerBoxMobile.css({ opacity: 1 })
}
<?php
function horaEvento($duracao,$lang){
  $inicioEvento = get_the_time('H');
  $terminoEvento = get_the_time('H') + $duracao;

  $linguaSimples = substr($lang, 0, 2);

  $en = ($linguaSimples == 'en');
  $es = ($linguaSimples == 'es');
  $pt = ($linguaSimples == 'pt');

  $inicioEvento = $en ? horaEn($inicioEvento) : $inicioEvento.'h';
  $terminoEvento = $en ? horaEn($terminoEvento) : $terminoEvento.'h';

  $frase;
  if($en){
    $frase = $inicioEvento.' to '.$terminoEvento;
  } elseif($es) {
    $frase = $inicioEvento.' a '.$terminoEvento;
  } elseif($pt) {
    $frase = $inicioEvento.'  às '.$terminoEvento;
  }

  return $frase;
}

function horaEn($hora){
  return ($hora > 12) ? ($hora-12).'p.m.' : $hora.'a.m.';
}

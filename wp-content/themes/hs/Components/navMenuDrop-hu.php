<div class="nav--menu__drop">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul>
					<li>
						<span><?php echo $state['helloaustralia'][$lang];?></span>
						<ul>
							<li>
								<a href="<?php echo home_url();?>/hu/sobre"><?php echo $state['sobre'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/servicos"><?php echo $state['servicos'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/parceiros"><?php echo $state['parceiros'][$lang];?></a>
							</li>
						</ul>
					</li>
					<li>
						<span><?php echo $state['estudenaaustralia'][$lang];?></span>
						<ul>
							<li>
								<a href="<?php echo home_url();?>/hu/osestadosunidos"><?php echo $state['osestadosunidos'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/cidades"><?php echo $state['cidades'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/cursos"><?php echo $state['cursos'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/destinos"><?php echo $state['destinos'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/acomodacoes"><?php echo $state['acomodacoes'][$lang];?></a>
							</li>
						</ul>
					</li>
					<li>
						<span><?php echo $state['informacoes'][$lang];?></span>
						<ul>
							<li>
								<a href="<?php echo home_url();?>/hu/vistosedocumentos"><?php echo $state['vistosedocumentos'][$lang];?></a>
							</li>
							<!--li>
								<a href="<?php echo home_url();?>/hu/trabalho"><?php echo $state['trabalho'][$lang];?></a>
							</li-->
						</ul>
					</li>
					<li>
						<span><?php echo $state['promocoes'][$lang];?></span>
						<ul>
							<li>
								<a href="<?php echo home_url();?>/hu/promocoes-intercambio"><?php echo $state['intercambio'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/promocoes-passagens"><?php echo $state['passagens'][$lang];?></a>
							</li>
						</ul>
					</li>
					<li>
						<span><?php echo $state['orcamento'][$lang];?></span>
						<ul>
							<li>
								<a href="<?php echo home_url();?>/hu/orcamento-intercambio"><?php echo $state['intercambio'][$lang];?></a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu/orcamento-passagens"><?php echo $state['passagens'][$lang];?></a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

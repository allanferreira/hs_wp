<div class="homehaView">
  <?php
  $content = [];
  $cats = [get_cat_ID('he'), get_cat_ID('home')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "galeria" => [
        "ptbr" => get_field('he-home-ptbr-galeria', $post->ID),
        "enus" => get_field('he-home-enus-galeria', $post->ID),
      ],
      "titulo" => [
        "ptbr" => get_field('he-home-ptbr-titulo', $post->ID),
        "enus" => get_field('he-home-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('he-home-ptbr-conteudo', $post->ID),
        "enus" => get_field('he-home-enus-conteudo', $post->ID)
      ],
      "video" => [
        "titulo" => [
          "ptbr" => get_field('he-home-ptbr-video-titulo', $post->ID),
          "enus" => get_field('he-home-enus-video-titulo', $post->ID)
        ],
        "conteudo" => [
          "ptbr" => get_field('he-home-ptbr-video-conteudo', $post->ID),
          "enus" => get_field('he-home-enus-video-conteudo', $post->ID)
        ],
        "link" => [
          "ptbr" => get_field('he-home-ptbr-video-link', $post->ID),
          "enus" => get_field('he-home-enus-video-link', $post->ID)
        ]
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['galeria'],
    "banner-destino" => 'he',
    "saibamais" => [
      "ptbr" => "Saiba mais",
      "enus" => "More"
    ]
  ], 'banner', LANGUAGE);
  ?>

	<?php
	$component = new Component;
	$component->create([
		"statics" => [
      "titulo" => [
        "ptbr" => "Onde você quer estudar?",
        "enus" => "Onde você quer estudar?"
      ],
      "maiscidades" => [
        "ptbr" => "Mais cidades",
        "enus" => "Mais cidades"
      ],
      "saibamais" => [
        "ptbr" => "Saiba mais",
        "enus" => "Saiba mais"
      ]
    ]
], 'onde-voce-quer-estudar', LANGUAGE, 'Views/HE/Home/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"statics" => [
      "contento" => [
        "ptbr" => "As melhores ofertas para\nas melhores cidades\nda Inglaterra. Aproveite!",
        "enus" => "As melhores ofertas para\nas melhores cidades\nda Inglaterra. Aproveite!"
      ],
      "vejamaisofertas" => [
        "ptbr" => "Veja mais ofertas",
        "enus" => "Veja mais ofertas"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ],
      "promocoes" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ]
    ]
], 'promocoes', LANGUAGE, 'Views/HE/Home/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
    "content" => $content['video']
], 'pensando-em-ir-para-a-australia', LANGUAGE, 'Views/HE/Home/Parts');
  ?>

	<?php
	$component = new Component;
	$component->create([
		"titulo" => [
      "ptbr" => "Conheça outros destinos",
      "enus" => "Conheça outros destinos"
    ]
], 'conheca-outros-destinos', LANGUAGE, 'Views/HE/Home/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([], 'travel', LANGUAGE, 'Views/HE/Home/Parts');
	?>
</div>

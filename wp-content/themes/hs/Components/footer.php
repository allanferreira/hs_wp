<section class="certificado">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>A Hello Study é certificada por:
				<ul>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/1.png"></li>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/2.png"></li>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/3.png"></li>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/4.png"></li>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/5.png"></li>
					<li><img src="<?=bloginfo('template_url')?>/img/footer/certificado/6.png"></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="footer">
	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<span>Receba novidades por e-mail:</span>
					<div class="newsletter__box">
						<form>
							<input type="email" placeholder="Digite aqui o seu e-mail"/>
							<input type="submit" value="Cadastre-se"/>
							<div class="h--clear"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="newsletter__line"></div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<nav class="h--flex-center">
					<div class="logo">
						<a href="<?php echo home_url();?>">
							<img src="<?php echo bloginfo('template_url');?>/img/hsLogoFooter.png"/>
						</a>
					</div>
					<div class="h--flex-col h--txt-center">
						<ul class="menu">
							<li>
								<a href="<?php echo home_url();?>">Hello Study</a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/ha">Hello Australia</a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hc">Hello Canada</a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/he">Hello England</a>
							</li>
							<li>
								<a href="<?php echo home_url();?>/hu">Hello Usa</a>
							</li>
						</ul>
					</div>
					<div class="h--txt-right">
						<ul class="social">
							<li>
								<a href="https://www.facebook.com/HelloStudy/" target="_blank" class="icon--social-facebook"></a>
							</li>
							<li>
								<a href="https://www.instagram.com/hellostudy/" target="_blank" class="icon--social-instagram"></a>
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCQGcubj6n8rEfoVVD6Vbe1w" target="_blank" class="icon--social-twitter"></a>
							</li>
							<li>
								<a href="https://www.linkedin.com/company-beta/7602969/" target="_blank" class="icon--social-linkedin"></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-lg-offset-2 copy">
				2015 - 2017 © Copyright - Hello Study, Todos os Direitos Reservados | Desenvolvido por Magor
			</div>
			<div class="col-lg-2 col-md-4">
				<?php
				$component = new Component;
				$component->create([
					"linguaDropdown" => "PT",
					"content" => [
						"brasil" => [
							"ptbr" => "Brasil",
							"enus" => "Brazil"
						],
						"colombia" => [
							"ptbr" => "Colômbia",
							"enus" => "Colômbia"
						],
						"peru" => [
							"ptbr" => "Peru",
							"enus" => "Peru"
						],
						"america" => [
							"ptbr" => "EUA",
							"enus" => "EUA"
						]
					]
				], 'dropDownLang', LANGUAGE);
				?>
			</div>
		</div>
	</div>
</section>

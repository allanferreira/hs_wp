<?php
require_once "../wp-load.php";
require_once "../model/Paginacao.php";


class Eventos extends Paginacao {

    public function monta($paginaRequisitada, $lista, $lang) {

        $content = [];
		$query = new WP_Query([
            'post_type' => $this->requisita,
            'posts_per_page' => $lista,
            'paged' => $paginaRequisitada
        ]);

		if ( $query->have_posts() ):
    		while ( $query->have_posts() ) : $query->the_post();
                $content = [
    				"imagem" => get_field('type-eventos-imagem', $post->ID),
    				"titulo" => [
    					"ptbr" => get_field('type-eventos-ptbr-titulo', $post->ID),
    					"enus" => get_field('type-eventos-enus-titulo', $post->ID)
    				],
    				"conteudo" => [
    					"ptbr" => get_field('type-eventos-ptbr-conteudo', $post->ID),
    					"enus" => get_field('type-eventos-enus-conteudo', $post->ID)
    				],
    				"duracao" => get_field('type-eventos-duracao', $post->ID),
    				"local" => [
    					"ptbr" => get_field('type-eventos-ptbr-local', $post->ID),
    					"enus" => get_field('type-eventos-enus-local', $post->ID)
    				]
    			];
                $state = [
                    "content" => [
                        "local" => [
        					"ptbr" => 'Local',
        					"enus" => 'Local'
        				],
        				"horario" => [
        					"ptbr" => 'Horário',
        					"enus" => 'Schedule'
        				]
                    ]
                ];
                ?>

                <div class="item">
        			<div class="container">
        				<div class="row">
        					<div class="col-md-2 col-xs-3">
        						<time class="data" datetime="<?php echo get_the_date('Y/m/d');?>">
        							<span class="mes"><?=dateMouthLang($lang)?></span>
        							<span class="dia"><?=get_the_date('d')?></span>
        						</time>
        					</div>
        					<div class="col-md-5 col-xs-9">
        						<img class="img-response" src="<?=$content['imagem']['sizes']['eventos']?>"/>
        					</div>
        					<div class="col-md-4 col-xs-9 col-md-offset-0 col-xs-offset-3">
        						<div class="contenttext">
        							<h1><?=$content['titulo'][$lang]?></h1>
        							<div class="info">
        								<span class="item">
        									<?=$state['content']['local'][$lang]?>
        									<span class="val"><?=$content['local'][$lang]?></span>
        								</span>
        								<span class="item">
        									<?=$state['content']['horario'][$lang]?>
        									<span class="val"><?=horaEvento($content['duracao'],$lang)?></span>
        								</span>
        							</div>
        			              <p><?=reduzirTexto($content['conteudo'][$lang], 20, '') ?></p>
        			              <a href="<?php the_permalink()?>" class="leiamais">Leia mais +</a>
        			            </div>
        					</div>
        				</div>
        			</div>
        		</div>

                <?php
            endwhile;
        endif;
        wp_reset_postdata();

    }

}

<div class="parceirosView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hu'), get_cat_ID('parceiros')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hu-parceiros-titulo" => [
  			"parceiros" => [
  				"ptbr" => get_field('hu-parceiros-parceiros-ptbr-titulo', $post->ID),
  				"enus" => get_field('hu-parceiros-parceiros-enus-titulo', $post->ID)
  			],
  			"escolas" => [
  				"ptbr" => get_field('hu-parceiros-escolas-ptbr-titulo', $post->ID),
  				"enus" => get_field('hu-parceiros-escolas-enus-titulo', $post->ID)
  			]
  		],
      "hu-parceiros-marcas" => [
        "parceiros" => get_field('hu-parceiros-parceiros-marcas', $post->ID),
  			"escolas" => get_field('hu-parceiros-parceiros-escolas', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'parceiros', LANGUAGE, 'Views/HU/Parceiros/Parts');
  ?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'escolas', LANGUAGE, 'Views/HU/Parceiros/Parts');
	?>
</div>

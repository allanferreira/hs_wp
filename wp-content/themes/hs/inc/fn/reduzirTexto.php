<?php
function reduzirTexto($texto, $limite, $retorno = '...'){
	$reduzido = [];
	$completo = strip_tags($texto);
	$completo = explode(" ", $completo);

	for ($i=0; $i < $limite; $i++) {
		$reduzido[$i] = $completo[$i];
	}

	$reduzido = implode(" ", $reduzido);
	return strip_tags($reduzido.$retorno);
}

<?php
function valorPromocoes($valor,$lang){
  $moeda;
  if($lang == 'ptbr'){
    $moeda = 'R$';
    $valor = explode(',',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>,{$valor[1]}</span><span class='ast'>*</span>";
  } elseif($lang == 'enus') {
    $moeda = 'US$';
    $valor = explode('.',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>.{$valor[1]}</span><span class='ast'>*</span>";
  } elseif($lang == 'enca') {
    $moeda = 'CA$';
    $valor = explode('.',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>.{$valor[1]}</span><span class='ast'>*</span>";
  } elseif($lang == 'esco') {
    $moeda = 'COL$';
    $valor = explode(',',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>,{$valor[1]}</span><span class='ast'>*</span>";
  } elseif($lang == 'espe') {
    $moeda = 'PEN';
    $valor = explode(',',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>,{$valor[1]}</span><span class='ast'>*</span>";
  } elseif($lang == 'eses') {
    $moeda = '€';
    $valor = explode(',',$valor);
    $valor = "<span class='moeda'>{$moeda}</span><span class='{$lang}'>{$valor[0]}</span><span class='cents'>,{$valor[1]}</span><span class='ast'>*</span>";
  }

  return $valor;
}

<?php
/**
	* Para adicionar novas linguagens é necessário:
	*
	* 1. Fazer manutenção nas entragas de conteúdo
	*    das queries dos componentes
	*
	* 2. Adicionar novas categorias de idiomas no Wordpress
	*
	* 3. Acrescentar novos campos de idioma nos grupos do ACF
	*
	* 4. Fazer manutenção das seguintes funções:
	*
	* 		a. dateLang() // usada em templates como: Views/HS/Blog/Parts/blog.php
	* 		b. dateMouthLang() // usada em templates como: Views/HS/Eventos/Parts/eventos.php
	* 		c. horaEvento() // usada em templates como: Views/HS/Eventos/Parts/eventos.php
	*/

require_once( __DIR__ . '/inc/helpers/Component.php');
require_once( __DIR__ . '/inc/helpers/Type.php');
require_once( __DIR__ . '/inc/helpers/ThumbSize.php');
require_once( __DIR__ . '/inc/helpers/customPagination.php');

require_once( __DIR__ . '/inc/fn/reduzirTexto.php');
require_once( __DIR__ . '/inc/fn/dateLang.php');
require_once( __DIR__ . '/inc/fn/dateMouthLang.php');
require_once( __DIR__ . '/inc/fn/horaEvento.php');
require_once( __DIR__ . '/inc/fn/montaUrlMapa.php');
require_once( __DIR__ . '/inc/fn/valorPromocoes.php');

require_once( __DIR__ . '/inc/admin.php');
require_once( __DIR__ . '/inc/setup.php');
//require_once( __DIR__ . '/inc/acf.php');

<section class="promocoespassagenshaView">
  <div class="nav--menu-height-single"></div>
  <?php
  $content = [];
  $cats = [get_cat_ID('he'), get_cat_ID('promocoespassagens')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "he-promocoespassagens-texto" => [
          "ptbr" => get_field('he-promocoespassagens-ptbr-texto', $post->ID),
          "enus" => get_field('he-promocoespassagens-enus-texto', $post->ID)
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>
  <?php
  $component = new Component;
  $component->create([
    "texto" => $content["he-promocoespassagens-texto"],
    "static" => [
      "titulo" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ],
      "passagem" => [
        "ptbr" => "Passagem",
        "enus" => "Passagem"
      ]
    ]
], 'item', LANGUAGE, 'Views/HE/Promocoespassagens/Parts');
	?>
</section>

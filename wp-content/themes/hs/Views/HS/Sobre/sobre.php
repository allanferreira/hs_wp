<div class="sobreView">
	<div class="nav--menu-height"></div>

	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('sobre')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hs-sobre-a-hello-study" => [
				"image" => [
					"ptbr" => get_field('hs-sobre-a-hello-study-ptbr-imagem', $post->ID),
					"enus" => get_field('hs-sobre-a-hello-study-enus-imagem', $post->ID)
				],
				"titulo" => [
					"ptbr" => get_field('hs-sobre-a-hello-study-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-sobre-a-hello-study-enus-titulo', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('hs-sobre-a-hello-study-ptbr-conteudo', $post->ID),
					"enus" => get_field('hs-sobre-a-hello-study-enus-conteudo', $post->ID)
				]
			],
			"hs-sobre-servicos" => [
				"titulo" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-titulo', $post->ID)
				],
				"item-1" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-1', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-1', $post->ID)
				],
				"item-2" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-2', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-2', $post->ID)
				],
				"item-3" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-3', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-3', $post->ID)
				],
				"item-4" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-4', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-4', $post->ID)
				],
				"item-5" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-5', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-5', $post->ID)
				],
				"item-6" => [
					"ptbr" => get_field('hs-sobre-servicos-ptbr-item-6', $post->ID),
					"enus" => get_field('hs-sobre-servicos-enus-item-6', $post->ID)
				]
			],
			"hs-sobre-o-grupo" => [
				"titulo" => [
					"ptbr" => get_field('hs-sobre-o-grupo-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-sobre-o-grupo-enus-titulo', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('hs-sobre-o-grupo-ptbr-conteudo', $post->ID),
					"enus" => get_field('hs-sobre-o-grupo-enus-conteudo', $post->ID)
				]
			],
			"hs-sobre-nossa-equipe" => [
				"titulo" => [
					"ptbr" => get_field('hs-sobre-nossa-equipe-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-sobre-nossa-equipe-enus-titulo', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('hs-sobre-nossa-equipe-ptbr-conteudo', $post->ID),
					"enus" => get_field('hs-sobre-nossa-equipe-enus-conteudo', $post->ID)
				]
			],
			"hs-sobre-parceiros" => [
				"titulo" => [
					"ptbr" => get_field('hs-sobre-parceiros-ptbr-titulo', $post->ID),
					"enus" => get_field('hs-sobre-parceiros-enus-titulo', $post->ID)
				]
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"nostatic" => "nostatic",
		"image" => $content['hs-sobre-a-hello-study']['image']
	], 'banner-static', LANGUAGE);
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-sobre-a-hello-study'],
		"class" => "hs-sobre-a-hello-study"
	], 'sectionText', LANGUAGE);
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-sobre-servicos'],
		"servicosCurso" => [
			"ptbr" => "Curso",
			"enen" => "Curso",
			"enau" => "Curso",
			"espe" => "Curso",
			"esco" => "Curso"
		],
		"servicosAcomodacao" => [
			"ptbr" => "Acomodação",
			"enen" => "Acomodação",
			"enau" => "Acomodação",
			"espe" => "Acomodação",
			"esco" => "Acomodação"
		],
		"servicosVisto" => [
			"ptbr" => "Visto",
			"enen" => "Visto",
			"enau" => "Visto",
			"espe" => "Visto",
			"esco" => "Visto"
		],
		"servicosPassagemAerea" => [
			"ptbr" => "Passagem Aérea",
			"enen" => "Passagem Aérea",
			"enau" => "Passagem Aérea",
			"espe" => "Passagem Aérea",
			"esco" => "Passagem Aérea"
		],
		"servicosAssistenciaViagem" => [
			"ptbr" => "Assistência Viagem",
			"enen" => "Assistência Viagem",
			"enau" => "Assistência Viagem",
			"espe" => "Assistência Viagem",
			"esco" => "Assistência Viagem"
		],
		"servicosCartaoMultiMoeda" => [
			"ptbr" => "Cartão Multi-moedas",
			"enen" => "Cartão Multi-moedas",
			"espe" => "Cartão Multi-moedas",
			"esco" => "Cartão Multi-moedas"
		],
	], 'servicos', LANGUAGE, 'Views/HS/Sobre/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-sobre-o-grupo'],
	], 'o-grupo', LANGUAGE, 'Views/HS/Sobre/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-sobre-nossa-equipe'],
	], 'nossa-equipe', LANGUAGE, 'Views/HS/Sobre/Parts');
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content['hs-sobre-parceiros'],
	], 'parceiros', LANGUAGE, 'Views/HS/Sobre/Parts');
	?>
</div>

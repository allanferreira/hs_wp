<section class="promocoesinterhaView">
  <div class="nav--menu-height-single"></div>
  <?php
  $content = [];
  $cats = [get_cat_ID('hc'), get_cat_ID('promocoesintercambio')];
  $query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
  while ( $query->have_posts() ): $query->the_post();

    $content = [
      "hc-promocoesintercambio-texto" => [
          "ptbr" => get_field('hc-promocoesintercambio-ptbr-texto', $post->ID),
          "enus" => get_field('hc-promocoesintercambio-enus-texto', $post->ID)
      ]
    ];

  endwhile; wp_reset_postdata();
  ?>
  <?php
  $component = new Component;
  $component->create([
    "texto" => $content["hc-promocoesintercambio-texto"],
    "static" => [
      "titulo" => [
        "ptbr" => "Promoções",
        "enus" => "Promoções"
      ],
      "estouinteressado" => [
        "ptbr" => "Estou interessado",
        "enus" => "Estou interessado"
      ]
    ]
], 'item', LANGUAGE, 'Views/HC/Promocoesintercambio/Parts');
	?>
</section>

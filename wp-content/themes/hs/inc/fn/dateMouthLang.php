<?php
function dateMouthLang($data, $lang){

	$meses = [
		'01' => [
			'ptbr' => 'Jan',
			'enus' => 'Jan',
			'eses' => 'Enuse'
		],
		'02' => [
			'ptbr' => 'Fev',
			'enus' => 'Feb',
			'eses' => 'Feb'
		],
		'03' => [
			'ptbr' => 'Mar',
			'enus' => 'Mar',
			'eses' => 'Mar'
		],
		'04' => [
			'ptbr' => 'Abr',
			'enus' => 'Apr',
			'eses' => 'Abr'
		],
		'05' => [
			'ptbr' => 'Mai',
			'enus' => 'May',
			'eses' => 'May'
		],
		'06' => [
			'ptbr' => 'Jun',
			'enus' => 'Jun',
			'eses' => 'Jun'
		],
		'07' => [
			'ptbr' => 'Jul',
			'enus' => 'Jul',
			'eses' => 'Jul'
		],
		'08' => [
			'ptbr' => 'Ago',
			'enus' => 'Aug',
			'eses' => 'Ago'
		],
		'09' => [
			'ptbr' => 'Set',
			'enus' => 'Sep',
			'eses' => 'Sep'
		],
		'10' => [
			'ptbr' => 'Out',
			'enus' => 'Oct',
			'eses' => 'Oct'
		],
		'11' => [
			'ptbr' => 'Nov',
			'enus' => 'Nov',
			'eses' => 'Nov'
		],
		'12' => [
			'ptbr' => 'Dez',
			'enus' => 'Dec',
			'eses' => 'Dic'
		]
	];

	$pattern = "/(\d{2})\/(\d{2})\/\d{4}/";

	preg_match($pattern, $data, $dataReg);
	
	$dia = $dataReg[1];
	$nMes = $dataReg[2];
	$mes = $meses[$nMes][$lang];

	$template = "
		<span class='mes'>$mes</span>
		<span class='dia'>$dia</span>
	";
	return $template;
}

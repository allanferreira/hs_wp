const Vue = require('vue/dist/vue')
const VeeValidate = require('vee-validate')
const VueResource = require('vue-resource')

Vue.use(VeeValidate)
Vue.use(VueResource)

const contato = new Vue({
    el: '#contato',
    data: {
        msg: "",
        assunto: "",
        nome: "",
        email: "",
        telefone: "",
        mensagem: "",

    },
    methods: {
        validate() {
            this.$validator.validateAll()
                .then(res => {

                    let assunto = this.assunto
                    let nome = this.nome
                    let email = this.email
                    let telefone = this.telefone
                    let mensagem = this.mensagem


                    let address = `http://birmann.magor.com.br/statics/phpmailer/?action=send&assunto=${assunto}&nome=${nome}&email=${email}&telefone=${telefone}&mensagem=${mensagem}`
                    this.$http.get(address)
                        .then(res => res.text())
                        .then(res => {
                            this.msg = res
                        })
                })
                .catch(err => {console.log(err)})
        }
    }
})

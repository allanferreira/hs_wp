<div class="contatoView">
  <div class="nav--menu-height"></div>
	<?php

	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('trabalheconosco')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

    $content = [
      "hs-trabalheconosco-trabalheconosco" => [
        "titulo" => [
          "ptbr" => get_field('hs-trabalheconosco-trabalheconosco-ptbr-titulo', $post->ID),
          "enus" => get_field('hs-trabalheconosco-trabalheconosco-enus-titulo', $post->ID)
        ],
        "conteudo" => [
          "ptbr" => get_field('hs-trabalheconosco-trabalheconosco-ptbr-conteudo', $post->ID),
          "enus" => get_field('hs-trabalheconosco-trabalheconosco-enus-conteudo', $post->ID)
        ],
        "nossasunidades" => [
          "ptbr" => get_field('hs-trabalheconosco-trabalheconosco-ptbr-nossasunidades', $post->ID),
          "enus" => get_field('hs-trabalheconosco-trabalheconosco-enus-nossasunidades', $post->ID)
        ]
      ],
      "hs-trabalheconosco-formulario" => [
        "ptbr" => get_field('hs-trabalheconosco-formulario-ptbr-assuntoemail', $post->ID),
        "enus" => get_field('hs-trabalheconosco-formulario-enus-assuntoemail', $post->ID)
      ],
      "hs-trabalheconosco-nossasunidades" => [
        "ptbr" => get_field('hs-trabalheconosco-nossasunidades-ptbr-enderecos', $post->ID),
        "enus" => get_field('hs-trabalheconosco-nossasunidades-enus-enderecos', $post->ID)
      ]
    ];

	endwhile; wp_reset_postdata();
	?>

	<?php
  $component = new Component;
	$component->create([
		"hs-trabalheconosco-trabalheconosco" => $content["hs-trabalheconosco-trabalheconosco"],
		"hs-trabalheconosco-formulario" => $content["hs-trabalheconosco-formulario"],
    "statics" => [
      "assunto" => [
        "ptbr"=>"Assunto",
        "enus"=>"Assunto"
      ],
      "escolhaumassunto" => [
        "ptbr"=>"Escolha um assunto",
        "enus"=>"Escolha um assunto"
      ],
      "nome" => [
        "ptbr"=>"Nome",
        "enus"=>"Nome"
      ],
      "email" => [
        "ptbr"=>"Email",
        "enus"=>"Email"
      ],
      "telefone" => [
        "ptbr"=>"Telefone",
        "enus"=>"Telefone"
      ],
      "mensagem" => [
        "ptbr"=>"Mensagem",
        "enus"=>"Mensagem"
      ],
      "enviar" => [
        "ptbr"=>"enviar",
        "enus"=>"enviar"
      ],
      "iframe" => [
        "ptbr" => "https://helloaustralia.1enrol.com/form/43b0de5b-a4f1-49c2-8b9d-a664013a91ce",
        "enus" => "https://helloaustralia.1enrol.com/form/43b0de5b-a4f1-49c2-8b9d-a664013a91ce"
      ]
    ]
], 'formulario', LANGUAGE, 'Views/HS/Trabalheconosco/Parts');
	?>
</div>

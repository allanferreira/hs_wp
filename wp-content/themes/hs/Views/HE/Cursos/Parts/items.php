<div class="ha-servicos-items">
  <ul class="items">
    <?php
    $item = $state['content'][$lang];
    $count = 0;
    while(list ($key, $val) = each ($item)):
    $count = ($count==2) ? 0 : $count
    ?>
    <li class="<?php if($count==1){ echo 'color';} ?>">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="item">
              <div class="item-img">
                <img src="<?php echo $item[$key]["he-cursos-$lang-item-icone"]["url"];?>">
              </div>
              <div class="item-content">
                <h2><?php echo $item[$key]["he-cursos-$lang-item-titulo"];?></h2>
                <p><?php echo $item[$key]["he-cursos-$lang-item-conteudo"];?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
    <?php
    $count++;
    endwhile;
    ?>
  </ul>
</div>

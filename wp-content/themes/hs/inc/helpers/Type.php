<?php
class Type {
    public function __construct($name, $icon) {
		$slug = $this->Slugify($name);

	    register_post_type($slug, array(
			'menu_icon' => $icon,
	        'labels' => array(
	            'name' => $name,
	            'all_items' => 'Todos'
	        ),
	        'public' => true,
			'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail')
	    ));

	    register_taxonomy($slug.'-categories', array($slug), array(
	        'labels' => array(
	            'name' => 'Seções'
	        ),
	        'hierarchical' => true,
			'show_in_menu' => true,
	    	'show_in_admin_bar' => true,
	        'rewrite' => array('slug' => $slug)
	    ));
    }
	public function Slugify($text){

    setlocale(LC_ALL, 'pt_BR');

    // replace non letter or digits by -
     $text = preg_replace('~[^\pL\d]+~u', '-', $text);

     // transliterate
     $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

     // remove unwanted characters
     $text = preg_replace('~[^-\w]+~', '', $text);

     // trim
     $text = trim($text, '-');

     // remove duplicate -
     $text = preg_replace('~-+~', '-', $text);

     // lowercase
     $text = strtolower($text);

     if (empty($text)) {
       return 'n-a';
     }

     return $text;
	}
}

<style>
#chaves .st0{	
	fill: none;
	stroke: #999;
	stroke-width: 0.5;
	stroke-linecap: round;
	stroke-linejoin: round;
	stroke-miterlimit: 10;
}
</style>
<svg id="chaves" x="0px" y="0px" viewBox="0 0 31 158">
	<polyline class="st0" points="22,151.6 21.4,151.6 12.5,151.6 12.5,86.1 6.5,80.6 12.5,75 12.5,7.5 22,7.5 "/>
</svg>
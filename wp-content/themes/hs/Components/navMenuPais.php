<nav class="nav--menu__hsbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="h--table">
          <ul>
            <li>
              <a href="<?php echo home_url();?>">
                <img src="<?php bloginfo('template_url');?>/img/hsLogoPais.png">
              </a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/sobre"><?php echo $state['sobre'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/destinos"><?php echo $state['destinos'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/depoimentos"><?php echo $state['depoimentos'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/blog"><?php echo $state['blog'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/eventos"><?php echo $state['eventos'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/ciasaereas"><?php echo $state['ciasaereas'][$lang];?></a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/trabalhe-conosco">Trabalhe conosco</a>
            </li>
            <li>
              <a href="<?php echo home_url();?>/contato"><?php echo $state['contato'][$lang];?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>

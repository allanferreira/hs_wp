<div class="aaustraliaView">
	<div class="nav--menu-height-single"></div>
  <?php
  the_post();
  $content = [
    "sobre" => [
      "titulo" => [
        "ptbr" => get_field('he-ainglaterra-ptbr-titulo', $post->ID),
        "enus" => get_field('he-ainglaterra-enus-titulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('he-ainglaterra-ptbr-conteudo', $post->ID),
        "enus" => get_field('he-ainglaterra-enus-conteudo', $post->ID)
      ]
    ],
    "imgdestaque" => get_field('he-ainglaterra-imagem-destaque', $post->ID),
    "imgdetalhe" => get_field('he-ainglaterra-imagem-detalhe', $post->ID),
    "detalhes" => [
      "ptbr" => get_field('he-ainglaterra-ptbr-detalhes', $post->ID),
      "enus" => get_field('he-ainglaterra-enus-detalhes', $post->ID)
    ],
    "porqueestudar" => [
      "titulo" => [
        "ptbr" => get_field('he-ainglaterra-ptbr-porqueestudartitulo', $post->ID),
        "enus" => get_field('he-ainglaterra-enus-porqueestudartitulo', $post->ID)
      ],
      "conteudo" => [
        "ptbr" => get_field('he-ainglaterra-ptbr-porqueestudarconteudo', $post->ID),
        "enus" => get_field('he-ainglaterra-enus-porqueestudarconteudo', $post->ID)
      ]
    ]
  ];
  ?>

  <?php
	$component = new Component;
	$component->create([
    "image" => $content['imgdestaque']
], 'banner', LANGUAGE, 'Views/HE/Ainglaterra/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['sobre'],
    "class" => "ha-aaustralia-sobre"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
    "content" => $content['detalhes'],
    "image" => $content['imgdetalhe']
], 'detalhes', LANGUAGE, 'Views/HE/Ainglaterra/Parts');
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['porqueestudar'],
    "class" => "ha-aaustralia-porqueestudar"
  ], 'sectionText', LANGUAGE);
  ?>
</div>

<section class="trabalhohaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$content = [];
	$cats = [get_cat_ID('he'), get_cat_ID('trabalho')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"trabalho" => [
  			"conteudo" => [
    			"titulo" => [
    				"ptbr" => get_field('he-trabalho-ptbr-titulo', $post->ID),
    				"enus" => get_field('he-trabalho-enus-titulo', $post->ID)
    			],
    			"conteudo" => [
    				"ptbr" => get_field('he-trabalho-ptbr-conteudo', $post->ID),
    				"enus" => get_field('he-trabalho-enus-conteudo', $post->ID)
    			]
        ],
        "lista" => [
          "ptbr" => get_field('he-trabalho-ptbr-lista', $post->ID),
          "enus" => get_field('he-trabalho-enus-lista', $post->ID)
        ]
      ]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
  $component = new Component;
  $component->create([
    "content" => $content['trabalho']['conteudo'],
    "class" => "ha-trabalho"
  ], 'sectionText', LANGUAGE);
  ?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content['trabalho']['lista']
	], 'lista', LANGUAGE, 'Views/HE/Trabalho/Parts');
	?>
</section>

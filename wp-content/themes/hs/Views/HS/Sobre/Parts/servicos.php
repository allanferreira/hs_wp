<section class="section--text hs-sobre-servicos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
		<div class="row items">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconCurso', 'ptbr');
									?>
								</div>
							</div>
							<div>	
								<h1>
									<?php echo $state['servicosCurso'][$lang];?>
									<span></span>
								</h1>
								<p><?php echo $state['content']['item-1'][$lang];?></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconPassagemAerea', 'ptbr');
									?>
								</div>
							</div>
							<div>
								<h1>
									<?php echo $state['servicosPassagemAerea'][$lang];?>
									<span></span>	
								</h1>
								<p><?php echo $state['content']['item-4'][$lang];?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconAcomodacao', 'ptbr');
									?>
								</div>
							</div>
							<div>
								<h1>
									<?php echo $state['servicosAcomodacao'][$lang];?>
									<span></span>	
								</h1>
								<p><?php echo $state['content']['item-2'][$lang];?></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconAssistenciaViagem', 'ptbr');
									?>	
								</div>
							</div>
							<div>
								<h1>
									<?php echo $state['servicosAssistenciaViagem'][$lang];?>
									<span></span>	
								</h1>
								<p><?php echo $state['content']['item-5'][$lang];?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconVisco', 'ptbr');
									?>				
								</div>
							</div>
							<div>
								<h1>
									<?php echo $state['servicosVisto'][$lang];?>
									<span></span>	
								</h1>
								<p><?php echo $state['content']['item-3'][$lang];?></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="item">
							<div class="icon">
								<div>
									<?php
									$component = new Component;
									$component->create([], 'iconCartaoMultiMoeda', 'ptbr');
									?>		
								</div>
							</div>
							<div>
								<h1>
									<?php echo $state['servicosCartaoMultiMoeda'][$lang];?>
									<span></span>	
								</h1>
								<p><?php echo $state['content']['item-6'][$lang];?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
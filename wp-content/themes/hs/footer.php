		<?php
		$component = new Component;
		$component->create([], 'footer', LANGUAGE);
		?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo bloginfo('template_url');?>/dist/js/app.js"></script>
	</body>
</html>

<div class="orcamentodecursosView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('orcamentodecursos')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
      "static" => [
        "naosei" => [
          "ptbr" => "Não sei",
          "espe" => "No lo sé",
          "esco" => "No lo sé",
          "eses" => "No lo sé",
          "enau" => "I don`t know",
          "enus" => "I don`t know"
        ]
      ],
			"hs-orcamentodecursos-orcamentodecursos" => [
  			"titulo" => [
  				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-titulo', $post->ID),
  				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-titulo', $post->ID)
  			],
  			"form" => [
    			"au" => [
    				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-formulario-australia', $post->ID),
    				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-formulario-australia', $post->ID)
    			],
    			"ca" => [
    				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-formulario-canada', $post->ID),
    				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-formulario-canada', $post->ID)
    			],
    			"en" => [
    				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-formulario-england', $post->ID),
    				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-formulario-england', $post->ID)
    			],
    			"us" => [
    				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-formulario-usa', $post->ID),
    				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-formulario-usa', $post->ID)
    			],
    			"naosei" => [
    				"ptbr" => get_field('hs-orcamentodecursos-orcamentodecursos-ptbr-formulario-naosei', $post->ID),
    				"espe" => get_field('hs-orcamentodecursos-orcamentodecursos-espe-formulario-naosei', $post->ID)
    			]
  			]
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'orcamentodecursos', LANGUAGE, 'Views/HA/Orcamentodecursos/Parts');
	?>
</div>

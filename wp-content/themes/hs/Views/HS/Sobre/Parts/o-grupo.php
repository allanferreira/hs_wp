<section class="section--text hs-sobre-o-grupo">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
			<div class="col-md-6">
				<?php
				$component = new Component;
				$component->create([
					"content" => [
						"saibamais" => [
							"ptbr" => "Saiba mais",
							"enus" => "More"
						]
					]
				], 'quadro', LANGUAGE);
				?>
			</div>
		</div>
	</div>
</section>

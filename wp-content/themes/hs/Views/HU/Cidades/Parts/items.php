<div class="section--text ha-cidades">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title">
          <h1><?=$state['static']['titulo'][$lang]?></h1>
          <span></span>
        </div>
      </div>
    </div>
    <div class="row items">
      <?php
      $args = array(
        'post_type' => 'cidades',
        'posts_per_page' => -1,
        'tax_query' => array( array(
          'taxonomy' => 'cidades-categories',
          'field' => 'slug',
          'terms' => 'hu'
        ))
      );
      $content = [];
      $count = 0;
      $query = new WP_Query( $args );
      while ( $query->have_posts() ) : $query->the_post();
        $content = [
          "imagem" => get_field('type-cidades-imagem', $post->ID),
          "titulo" => [
            "ptbr" => get_field('type-cidades-ptbr-titulo', $post->ID),
            "enus" => get_field('type-cidades-enus-titulo', $post->ID)
          ],
          "conteudo" => [
            "ptbr" => get_field('type-cidades-ptbr-conteudo', $post->ID),
            "enus" => get_field('type-cidades-enus-conteudo', $post->ID)
          ]
        ];
        $count = $count == 3 ? 0 : $count;
        $lado = $count == 0 ? 'left' : ($count == 1 ? 'center' : 'right');
        $line = $count == 2 ? 'line' : '';
      ?>
        <div class="col-md-4">
          <div class="item <?=$lado?>">
            <img class="image" src="<?php echo $content['imagem']['sizes']['cidades'];?>">
            <h1><?php echo $content['titulo'][$lang];?></h1>
            <div class="content">
              <?php //echo $content['conteudo'][$lang];?>
              <?php echo reduzirTexto($content['conteudo'][$lang], 13, '...'); ?>
            </div>

            <a href="<?php echo the_permalink();?>">
              <button class="button--base-big button--gd-hs1"><?php echo $state['static']['saibamais'][$lang];?></button>
            </a>
          </div>
        </div>
        <div class="<?=$line?>"></div>
      <?php $count++; endwhile; wp_reset_postdata();?>
    </div>
  </div>
</div>

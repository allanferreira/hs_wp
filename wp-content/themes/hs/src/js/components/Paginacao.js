module.exports = {

    inicia(btn){

        this.btn = btn
        this.btnValue = $(this.btn).text()
        this.lista = btn.getAttribute('data-lista')
        this.requisita = btn.getAttribute('data-requisita')
        this.lang = btn.getAttribute('data-lang')
        this.regra = btn.getAttribute('data-regra')
        this.paginaRequisitada = parseInt( btn.getAttribute('data-requisitapagina') )
        this.paginaSequinte = this.paginaRequisitada+1
        this.middleBase = `../middleware/Paginacao.php?regra=${this.regra}&lang=${this.lang}&lista=${this.lista}`

        this.requisitaPagina()

    },

    preparaBotaoLoad(){

        this.btn.setAttribute('data-requisitapagina',this.paginaSequinte)

        $(this.btn)
            .text('')
            .addClass('bounce animated infinite')

    },

    preparaBotaoComum(){

        $(this.btn)
            .removeClass('bounce animated infinite')
            .text(this.btnValue)

    },

    requisitaPagina(){

        this.preparaBotaoLoad()

        let middle = `${this.middleBase}&requisita=${this.requisita}&paginaRequisitada=${this.paginaRequisitada}`
        fetch(middle, { method:'get' })
            .then(res => res.text())
            .then(res => {
                setTimeout(function(){

                    let items = document.querySelector('.paginacao-items')
                    items.innerHTML += res

                    this.preparaBotaoComum()
                    if(!this.existePost())
                        return

                }.bind(this), 1000)
            })
    },

    existePost(){

        let retorno

        let middle = `${this.middleBase}&requisita=${this.requisita}&paginaRequisitada=${this.paginaSequinte}`
        fetch(middle, { method:'get' })
            .then(res => res.text())
            .then(res => {
                if(!res){
                    this.btn.setAttribute('style','display:none')
                    retorno = !retorno
                }
            })

        return !retorno ? !retorno : retorno

    }

}

<?php
function dateLang($lang){
	$invertido = ($lang == 'enus' or $lang == 'enca');
	return ($invertido) ? get_the_date('Y/m/d') : get_the_date('d/m/Y');
}

<?php if ($state['content']['titulo'][$lang] != "") { ?>
<section class="section--text <?php echo $state['class'];?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>

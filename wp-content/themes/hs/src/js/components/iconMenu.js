function menuIcon(){

	let w = $(window).width()

	if(w < 993){
		$('.icon--menu, .icon--menu-label').click(function(){
			$('.icon--menu').toggleClass('open')
			$('.nav--menu__drop').toggleClass('open animated bounceInDown')
		})
	} else {
		$('.icon--menu, .icon--menu-label').hover(function(){
			$('.icon--menu').addClass('open')
			$('.nav--menu__drop').addClass('open animated bounceInDown')
		})
		$('.nav--menu').hover(function(){},function(){
			$('.icon--menu').removeClass('open')
			$('.nav--menu__drop').removeClass('open animated bounceInDown')
		})
	}
}

menuIcon()
$(window).resize(menuIcon())

<div class="section--text ha-promocoespassagens">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title">
          <h1><?=$state['static']['titulo'][$lang]?></h1>
          <span></span>
        </div>
      </div>
    </div>
    <div class="row items">
      <?php
      $args = array(
        'post_type' => 'promocoes-passagens',
        'posts_per_page' => -1,
        'tax_query' => array( array(
          'taxonomy' => 'promocoes-passagens-categories',
          'field' => 'slug',
          'terms' => 'ha'
        ))
      );
      $content = [];
      $count = 0;
      $query = new WP_Query( $args );
      while ( $query->have_posts() ) : $query->the_post();
        $content = [
          "imagem" => get_field('type-promocoespassagens-image', $post->ID),
          "logo" => get_field('type-promocoespassagens-logo', $post->ID),
          "cidade" => [
            "ptbr" => get_field('type-promocoespassagens-ptbr-cidade', $post->ID),
            "enus" => get_field('type-promocoespassagens-enus-cidade', $post->ID)
          ],
          "conteudo" => [
            "ptbr" => get_field('type-promocoespassagens-ptbr-conteudo', $post->ID),
            "enus" => get_field('type-promocoespassagens-enus-conteudo', $post->ID)
          ],
          "valor" => [
            "ptbr" => get_field('type-promocoespassagens-ptbr-valor', $post->ID),
            "enus" => get_field('type-promocoespassagens-enus-valor', $post->ID)
          ],
          "parcelas" => [
            "ptbr" => get_field('type-promocoespassagens-ptbr-parcelas', $post->ID),
            "enus" => get_field('type-promocoespassagens-enus-parcelas', $post->ID)
          ],
          "linkform" => [
            "ptbr" => get_field('type-promocoespassagens-ptbr-linkform', $post->ID),
            "enus" => get_field('type-promocoespassagens-enus-linkform', $post->ID)
          ]
        ];
        $count = $count == 2 ? 0 : $count;
        $lado = $count == 0 ? 'left' : 'right';
        $line = $count == 1 ? 'line' : '';
        $cidade = $content['cidade'][$lang];
      ?>
        <div class="col-md-6">
          <div class="item <?=$lado?>">
            <div class="h--relative">
              <img class="image" src="<?php echo $content['imagem']['sizes']['promocoes'];?>">
              <div class="logo">
                <div class="center">
                  <img src="<?php echo $content['logo']['sizes']['marcas'];?>">
                </div>
              </div>
            </div>
            <h1>
              <?php echo $state['static']['passagem'][$lang];?> -
              <?php echo $cidade->post_title;?>
            </h1>
            <div class="valor">
              <?php echo valorPromocoes($content['valor'][$lang], $lang);?>
            </div>
            <div class="parcelas">
              <strong><?php echo $content['parcelas'][$lang];?></strong>
              <strong><?php echo $content['fixas'][$lang];?></strong>
            </div>
            <div class="content">
              <?php echo $content['conteudo'][$lang];?>
            </div>

            <a href="<?php echo $content['linkform'][$lang];?>">
              <button class="button--base-big button--gd-ha1"><?php echo $state['static']['estouinteressado'][$lang];?></button>
            </a>
          </div>
        </div>
        <div class="<?=$line?>"></div>
      <?php $count++; endwhile; wp_reset_postdata();?>
      <?php if($count == 1){ ?>
      <div class="line"></div>
      <?php } ?>
    </div>
    
 
    
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="texto">
          <?php echo $state['texto'][$lang];?>
        </div>
      </div>
    </div>
  </div>
</div>

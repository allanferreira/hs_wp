<div class="nav--menu hu single">
	<?php
	$component = new Component;
	$component->create([
		"sobre" => [
			"ptbr" =>  'Sobre',
			"enus" =>  'Sobre',
			"espe" =>  'Sobre',
			"esco" =>  'Sobre',
			"enau" =>  'Sobre'
		],
		"destinos" => [
			"ptbr" =>  'Destinos',
			"enus" =>  'Destinos',
			"espe" =>  'Destinos',
			"esco" =>  'Destinos',
			"enau" =>  'Destinos'
		],
		"depoimentos" => [
			"ptbr" =>  'Depoimentos',
			"enus" =>  'Depoimentos',
			"espe" =>  'Depoimentos',
			"esco" =>  'Depoimentos',
			"enau" =>  'Depoimentos'
		],
		"blog" => [
			"ptbr" =>  'Blog',
			"enus" =>  'Blog',
			"espe" =>  'Blog',
			"esco" =>  'Blog',
			"enau" =>  'Blog'
		],
		"eventos" => [
			"ptbr" =>  'Eventos',
			"enus" =>  'Eventos',
			"espe" =>  'Eventos',
			"esco" =>  'Eventos',
			"enau" =>  'Eventos'
		],
		"ciasaereas" => [
			"ptbr" =>  'Cias Aéreas',
			"enus" =>  'Cias Aéreas',
			"espe" =>  'Cias Aéreas',
			"esco" =>  'Cias Aéreas',
			"enau" =>  'Cias Aéreas'
		],
		"contato" => [
			"ptbr" =>  'Contato',
			"enus" =>  'Contato',
			"espe" =>  'Contato',
			"esco" =>  'Contato',
			"enau" =>  'Contato'
		]
	], 'navMenuPais', LANGUAGE);
	?>
	<nav class="nav--menu__bar">
		<div class="container">
			<div class="row">
				<div class="col-md-12 h--flex h--flex-center">
					<div class="h--flex-col">
						<div class="nav--menu__bar__icon">
							<?php
							$component = new Component;
							$component->create([], 'iconMenu', LANGUAGE);
							?>
						</div>
					</div>
					<div class="h--flex-col h--txt-center">
						<a href="<?php echo home_url();?>/hu">
							<img class="logo" src="<?php bloginfo('template_url');?>/img/huLogo.png"/>
							<img class="logomin" src="<?php bloginfo('template_url');?>/img/huLogomin.png"/>
						</a>
					</div>
					<div class="h--flex-col h--txt-right">
						<a href="<?php echo home_url();?>/hu/orcamento-de-cursos?us">
							<button class="button--base orcamento">
								<?php echo $state['content']['orcamento'][$lang]; ?>
							</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</nav>
	<div class="h--relative">
		<?php
		$component = new Component;
		$component->create([
			"helloaustralia" => [
				"ptbr" =>  'Hello<br/>United States',
				"enus" =>  'Hello<br/>United States'
			],
			"sobre" => [
				"ptbr" =>  'Sobre',
				"enus" =>  'Sobre'
			],
			"servicos" => [
				"ptbr" =>  'Serviços',
				"enus" =>  'Serviços'
			],
			"parceiros" => [
				"ptbr" =>  'Parceiros',
				"enus" =>  'Parceiros'
			],
			"estudenaaustralia" => [
				"ptbr" =>  'Estude<br/>nos Estados Unidos',
				"enus" =>  'Estude<br/>nos Estados Unidos'
			],
			"osestadosunidos" => [
				"ptbr" =>  'Os Estados Unidos',
				"enus" =>  'Os Estados Unidos'
			],
			"cursos" => [
				"ptbr" =>  'Cursos',
				"enus" =>  'Cursos'
			],
			"cidades" => [
				"ptbr" =>  'Cidades',
				"enus" =>  'Cidades'
			],
			"acomodacoes" => [
				"ptbr" =>  'Acomodações',
				"enus" =>  'Acomodações'
			],
			"informacoes" => [
				"ptbr" =>  'Informações',
				"enus" =>  'Informações'
			],
			"vistosedocumentos" => [
				"ptbr" =>  'Vistos & Documentos',
				"enus" =>  'Vistos & Documentos'
			],
			"trabalho" => [
				"ptbr" =>  'Trabalho',
				"enus" =>  'Trabalho'
			],
			"promocoes" => [
				"ptbr" =>  'Promoções',
				"enus" =>  'Promoções'
			],
			"intercambio" => [
				"ptbr" =>  'Intercâmbio',
				"enus" =>  'Intercâmbio'
			],
			"passagens" => [
				"ptbr" =>  'Passagens',
				"enus" =>  'Passagens'
			],
			"orcamento" => [
				"ptbr" =>  'Orçamento',
				"enus" =>  'Orçamento'
			],
			"intercambio" => [
				"ptbr" =>  'Intercâmbio',
				"enus" =>  'Intercâmbio'
			],
			"passagens" => [
				"ptbr" =>  'Passagens',
				"enus" =>  'Passagens'
			]
		], 'navMenuDrop-hu', LANGUAGE);
		?>
	</div>
</div>

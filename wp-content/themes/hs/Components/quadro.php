<div class="quadro">
	<img src="<?php echo bloginfo('template_url');?>/img/quadro.jpg">
	<div class="banner__box">
		<div class="banner__box">
			<div class="banner__box__item australia">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/australiaLogo.png"/>
							<a href="<?php echo home_url();?>/ha">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['content']['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="http://placehold.it/392x276"/>
				<img class="sizeHover" src="http://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item canada">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/canadaLogo.png"/>
							<a href="<?php echo home_url();?>/hc">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['content']['saibamais'][$lang];?>
								</button> 
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="http://placehold.it/392x276"/>
				<img class="sizeHover" src="http://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item inglaterra">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/inglaterraLogo.png"/>
							<a href="<?php echo home_url();?>/he">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['content']['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="http://placehold.it/392x276"/>
				<img class="sizeHover" src="http://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item usa">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/usaLogo.png"/>
							<a href="<?php echo home_url();?>/hu">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['content']['saibamais'][$lang];?>
								</button> 
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="http://placehold.it/392x276"/>
				<img class="sizeHover" src="http://placehold.it/392x341"/>
			</div>
		</div>
	</div>
</div>
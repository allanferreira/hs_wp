<div class="nav--menu__drop">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if(has_category('hs') or is_home() or is_single()):?>
				<ul>
					<li>
						<a class="<?php if(has_category('sobre')){ echo 'active';}?>" href="<?php echo home_url();?>/sobre">
							<?php echo $state['content']['sobre'][$lang];?>
						</a>
					</li>
					<li>
						<a class="<?php if(has_category('destinos')){ echo 'active';}?>" href="<?php echo home_url();?>/destinos">
							<?php echo $state['content']['destinos'][$lang];?>
						</a>
					</li>
					<li>
						<a class="<?php if(has_category('depoimentos')){ echo 'active';}?>" href="<?php echo home_url();?>/depoimentos">
							<?php echo $state['content']['depoimentos'][$lang];?>
						</a>
					</li>
					<li>
						<a class="<?php if(has_category('blog')){ echo 'active';}?>" href="<?php echo home_url();?>/blog">
							<?php echo $state['content']['blog'][$lang];?>
						</a>
					</li>
					<li>
						<a class="<?php if(has_category('eventos')){ echo 'active';}?>" href="<?php echo home_url();?>/eventos">
							<?php echo $state['content']['eventos'][$lang];?>
						</a>
					</li>
					<li>
						<a class="<?php if(has_category('ciasaereas')){ echo 'active';}?>" href="<?php echo home_url();?>/ciasaereas">
							<?php echo $state['content']['ciasaereas'][$lang];?>
						</a>
					</li>
					<li>
					  <a href="<?php echo home_url();?>/trabalhe-conosco">
						  Trabalhe conosco
					  </a>
					</li>
					<li>
						<a class="<?php if(has_category('contato')){ echo 'active';}?>" href="<?php echo home_url();?>/contato">
							<?php echo $state['content']['contato'][$lang];?>
						</a>
					</li>
				</ul>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>

<?php
$args = array(
  'post_type' => 'promocoes-inter',
  'posts_per_page' => 1,
  'orderby' => 'rand',
  'tax_query' => array( array(
    'taxonomy' => 'promocoes-inter-categories',
    'field' => 'slug',
    'terms' => 'he'
  ))
);
$content = [];
$count = 0;
$query = new WP_Query( $args );
while ( $query->have_posts() ) : $query->the_post();
  $content = [
    "imagem" => get_field('type-promocoesint-image', $post->ID),
    "titulo" => [
      "ptbr" => get_field('type-promocoesint-ptbr-titulo', $post->ID),
      "enus" => get_field('type-promocoesint-enus-titulo', $post->ID)
    ],
    "cidade" => [
      "ptbr" => get_field('type-promocoesint-ptbr-cidade', $post->ID),
      "enus" => get_field('type-promocoesint-enus-cidade', $post->ID)
    ],
    "conteudo" => [
      "ptbr" => get_field('type-promocoesint-ptbr-conteudo', $post->ID),
      "enus" => get_field('type-promocoesint-enus-conteudo', $post->ID)
    ],
    "valor" => [
      "ptbr" => get_field('type-promocoesint-ptbr-valor', $post->ID),
      "enus" => get_field('type-promocoesint-enus-valor', $post->ID)
    ],
    "parcelas" => [
      "ptbr" => get_field('type-promocoesint-ptbr-parcelas', $post->ID),
      "enus" => get_field('type-promocoesint-enus-parcelas', $post->ID)
    ],
    "fixas" => [
      "ptbr" => get_field('type-promocoesint-ptbr-fixas', $post->ID),
      "enus" => get_field('type-promocoesint-enus-fixas', $post->ID)
    ],
    "detalhe" => [
      "ptbr" => get_field('type-promocoesint-ptbr-detalhe', $post->ID),
      "enus" => get_field('type-promocoesint-enus-detalhe', $post->ID)
    ],
    "linkform" => [
      "ptbr" => get_field('type-promocoesint-ptbr-linkform', $post->ID),
      "enus" => get_field('type-promocoesint-enus-linkform', $post->ID)
    ]
  ];
  $count = $count == 2 ? 0 : $count;
  $lado = $count == 0 ? 'left' : 'right';
?>
<section class="promocoesSection" style="background: url('<?=$content['imagem']['sizes']['promocoesBg']?>') no-repeat bottom right fixed; background-size: cover;">
  <div class="promocoesBg--dark"></div>
  <div class="promocoesBg--light"></div>
  <div class="container">
    <div class="col-lg-6 col-md-6">
      <div class="title">
        <h1><?php echo $state['statics']['promocoes'][$lang]?></h1>
        <span></span>
        <pre><?php echo $state['statics']['contento'][$lang];?></pre>
        <a href="<?php echo home_url();?>/he/promocoes-intercambio">
          <?php echo $state['statics']['vejamaisofertas'][$lang];?>
        </a>
      </div>
    </div>
    <div class="col-lg-5 col-lg-offset-1 col-md-6 info hidden-xs">
      <div class="promocoesinterhaView">
        <div class="ha-promocoesintercambio">
          <div class="row items">
            <div class="col-lg-6">
              <div class="item <?=$lado?>">
                <h1>
                  <?php echo $content['titulo'][$lang];?>
                  <?php
                  $cidade = $content['cidade'][$lang];
                  if(!empty($cidade)){ echo "(".$cidade->post_title.")";}
                  ?>
                </h1>
                <div class="content">
                  <?php echo $content['conteudo'][$lang];?>
                </div>
                <div class="valor">
                  <?php echo valorPromocoes($content['valor'][$lang], $lang);?>
                </div>
                <div class="parcelas">
                  <strong><?php echo $content['parcelas'][$lang];?></strong>
                  <strong><?php echo $content['fixas'][$lang];?></strong>
                </div>
                <div class="detalhe">
                  <?php echo $content['detalhe'][$lang];?>
                </div>

                <a href="<?php echo $content['linkform'][$lang];?>">
                  <button class="button--base-big button--gd-hs1"><?php echo $state['statics']['estouinteressado'][$lang];?></button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $count++; endwhile; wp_reset_postdata();?>

<?php get_header();
$query = new WP_Query([
	'post_type' => 'page',
	'category_name' => 'hs',
	'post_per_page' => 1
]);
if ( $query->have_posts() )
	get_template_part('./Views/HS/Home/home');
get_footer();
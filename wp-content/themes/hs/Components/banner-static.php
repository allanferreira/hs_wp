<?php if(isset($state['nostatic'])):?>
<div class="banner-static" style="background-image: url(<?=$state['image'][$lang]['sizes']['wide'];?>)"></div>
<?php else:?>
<div class="banner-static" style="background-image: url(<?php echo bloginfo('template_url');?>/img/<?php echo $state['image']?>)"></div>
<?php endif;?>

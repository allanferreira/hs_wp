<section class="section--text ha-parceiros-escolas">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['hu-parceiros-titulo']['escolas'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
          <section class="ha-parceiros-marcas">
            <div class="paises">
              <div class="pais">
                <ul class="marcas">
                  <?php foreach ($state['content']['hu-parceiros-marcas']['escolas'] as $key => $imagem):?>
                    <li class="marca">
                      <div class="h--table">
                        <div class="h--table-cell">
                          <img src="<?php echo $imagem['sizes']['marcas'];?>"/>
                        </div>
                      </div>
                    </li>
                  <?php endforeach;?>
                </ul>
              </div>
            </div>
          </section>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
if($post->post_type == 'blog'):

	get_header();
	get_template_part('./Views/HS/Blog/single');

elseif($post->post_type == 'eventos'):

	get_header();
	get_template_part('./Views/HS/Eventos/single');

elseif($post->post_type == 'cidades'):

	$terms = wp_get_post_terms($post->ID,'cidades-categories')[0];
	if($terms->slug == 'ha'):

		get_header('ha');
		get_template_part('./Views/HA/Cidades/single');

	elseif($terms->slug == 'hc'):

		get_header('hc');
		get_template_part('./Views/HC/Cidades/single');

	elseif($terms->slug == 'he'):

		get_header('he');
		get_template_part('./Views/HE/Cidades/single');

	elseif($terms->slug == 'hu'):

		get_header('hu');
		get_template_part('./Views/HU/Cidades/single');

	endif;

endif;
get_footer();

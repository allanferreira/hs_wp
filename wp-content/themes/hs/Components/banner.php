<div class="banner <?php if($state['banner_box']){ echo 'home';}?>">
	<div class="banner__slider owl-carousel fadeIn animated">
		<?php
		$destino = $state['banner-destino'];
		foreach ($state['content'][$lang] as $banner):
		?>
			<div class="banner__slider__item slide" style="background-image:url(<?=$banner["$destino-home-$lang-galeria-imagem"]["sizes"]["bannerhome"]?>);">
					<div class="banner__slider__item__content">
						<div class="banner__slider__item__content__center">
							<img src="<?=$banner["$destino-home-$lang-galeria-texto"]["url"]?>">
						</div>
					</div>
			</div>
		<?php endforeach;?>
	</div>
	<div class="visible-md visible-lg">
		<div class="banner__box">
			<div class="banner__box__item australia">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/australiaLogo.png"/>
							<a href="<?php echo home_url();?>/ha">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item canada">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/canadaLogo.png"/>
							<a href="<?php echo home_url();?>/hc">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item inglaterra">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/inglaterraLogo.png"/>
							<a href="<?php echo home_url();?>/he">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item usa">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/usaLogo.png"/>
							<a href="<?php echo home_url();?>/hu">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
		</div>
	</div>
	<div class="visible-xs visible-sm">
		<div style="opacity: 0" class="mobileBox banner__box">
			<div class="banner__box__item australia">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/australiaLogo.png"/>
							<a href="<?php echo home_url();?>/ha">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item canada">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/canadaLogo.png"/>
							<a href="<?php echo home_url();?>/hc">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
		</div>
		<div style="opacity: 0" class="mobileBox banner__box">
			<div class="banner__box__item inglaterra">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/inglaterraLogo.png"/>
							<a href="<?php echo home_url();?>/he">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
			<div class="banner__box__item usa">
				<div class="banner__box__item__content">
					<div class="h--table">
						<div class="h--table-cell">
							<img class="banner__box__item__content__logo" src="<?php echo bloginfo('template_url');?>/img/usaLogo.png"/>
							<a href="<?php echo home_url();?>/hu">
								<button class="banner__box__item__content__button button--default">
									<?php echo $state['saibamais'][$lang];?>
								</button>
							</a>
						</div>
					</div>
				</div>
				<img class="size" src="https://placehold.it/392x276"/>
				<img class="sizeHover" src="https://placehold.it/392x341"/>
			</div>
		</div>
	</div>
</div>

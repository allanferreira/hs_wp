<div class="parceirosView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hc'), get_cat_ID('parceiros')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"hc-parceiros-titulo" => [
  			"parceiros" => [
  				"ptbr" => get_field('hc-parceiros-parceiros-ptbr-titulo', $post->ID),
  				"enus" => get_field('hc-parceiros-parceiros-enus-titulo', $post->ID)
  			],
  			"escolas" => [
  				"ptbr" => get_field('hc-parceiros-escolas-ptbr-titulo', $post->ID),
  				"enus" => get_field('hc-parceiros-escolas-enus-titulo', $post->ID)
  			]
  		],
      "hc-parceiros-marcas" => [
        "parceiros" => get_field('hc-parceiros-parceiros-marcas', $post->ID),
  			"escolas" => get_field('hc-parceiros-parceiros-escolas', $post->ID)
      ]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'parceiros', LANGUAGE, 'Views/HC/Parceiros/Parts');
  ?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'escolas', LANGUAGE, 'Views/HC/Parceiros/Parts');
	?>
</div>

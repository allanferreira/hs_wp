<section class="section--default blogSection">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['blogTitle'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
		<div class="row">
			<?php
			$args = array(
				'post_type' => 'blog',
				'posts_per_page' => 3
			);
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) : $query->the_post();
				$content = [
					"imagem" => get_field('type-blog-imagem', $post->ID),
					"titulo" => [
						"ptbr" => get_field('type-blog-ptbr-titulo', $post->ID),
						"enus" => get_field('type-blog-enus-titulo', $post->ID)
					],
					"conteudo" => [
						"ptbr" => get_field('type-blog-ptbr-conteudo', $post->ID),
						"enus" => get_field('type-blog-enus-conteudo', $post->ID)
					]
				];
			?>
				<div class="col-md-4 post">
					<div class="post__content">
						<div class="post__content__image">
							<div class="post__content__image__bg"></div>
							<img src="<?php echo $content["imagem"]["sizes"]["blogportable"];?>" />
						</div>
						<h1><?php echo $content['titulo'][$lang];?></h1>
						<time datetime="<?php echo get_the_date('Y/m/d');?>"><?php echo dateLang($lang); ?></time>
						<p><?php echo reduzirTexto($content['conteudo'][$lang], 20); ?></p>
						<a href="<?php the_permalink();?>">
							<?php echo $state['content']['leiamais'][$lang];?> +
						</a>
					</div>
				</div>
			<?php endwhile;?>
		</div>
	</div>
</section>

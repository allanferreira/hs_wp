<?php
$arrDepoimentos = $state['content']['depoimentosConteudo'][$lang];
$arrDepoimentosSize = count($arrDepoimentos);
$rand = array_rand($arrDepoimentos);
?>
<section class="section--default depoimentosSection">
	<div class="container h--relative">
		<img class="depoimentosDestaqueImage" src="<?=$arrDepoimentos[$rand]['hs-home-'.$lang.'-depoimentos-conteudo-imagem']['url']?>"/>
		<div class="row">
			<div class="col-md-offset-4 col-md-8">
				<div class="title">
					<h1><?php echo $state['content']['depoimentosTitle'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
		<div class="row h--no-scroll">
			<div class="col-md-offset-3 col-md-9">
				<div class="depoimentosDestaque">
					<?=$arrDepoimentos[$rand]['hs-home-'.$lang.'-depoimentos-conteudo-texto']?>
					<div class="blockquote1"></div>
					<div class="blockquote2"></div>
					<div class="button--base-big--section visible-md visible-lg ">
						<a href="<?php echo home_url();?>/depoimentos"><button class="button--base-big button--gd-hs2"><?php echo $state['content']['leiamais'][$lang];?></button></a>
					</div>
				</div>
				<div class="button--base-big--section visible-xs visible-sm ">
					<a href="<?php echo home_url();?>/depoimentos"><button class="button--base-big button--gd-hs2"><?php echo $state['content']['leiamais'][$lang];?></button></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="cidadeshaView">
  <div class="nav--menu-height-single"></div>

  <?php
	$component = new Component;
	$component->create([
    "static" => [
      "titulo" => [
        "ptbr" => "Cidades",
        "enus" => "Cidades"
      ],
      "saibamais" => [
        "ptbr" => "Saiba mais",
        "enus" => "Saiba mais"
      ]
    ],
		"content" => $content['items']
	], 'items', LANGUAGE, 'Views/HA/Cidades/Parts');
	?>
</section>

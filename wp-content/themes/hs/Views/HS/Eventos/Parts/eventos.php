<?php
/**
	* As categorias do post_type 'eventos' adicionam classes
	* além de servirem como filtro para a busca
	*/
?>
<section class="section--text hs-eventos-eventos">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
			</div>
		</div>
	</div>

	<div class="content">

		<?php
		$lista = 5;
		$args = array(
			'post_type' => 'eventos',
			'posts_per_page' => $lista
		);
		$content = [];
		$query = new WP_Query( $args );
		while ( $query->have_posts() ) : $query->the_post();
			$content = [
				"imagem" => get_field('type-eventos-imagem', $post->ID),
				"titulo" => [
					"ptbr" => get_field('type-eventos-ptbr-titulo', $post->ID),
					"enus" => get_field('type-eventos-enus-titulo', $post->ID)
				],
				"conteudo" => [
					"ptbr" => get_field('type-eventos-ptbr-conteudo', $post->ID),
					"enus" => get_field('type-eventos-enus-conteudo', $post->ID)
				],
				"horario" => [
					"ptbr" => get_field('type-eventos-ptbr-horario', $post->ID),
					"enus" => get_field('type-eventos-enus-horario', $post->ID)
				],
				"data" => [
					"ptbr" => get_field('type-eventos-ptbr-data', $post->ID),
					"enus" => get_field('type-eventos-enus-data', $post->ID)
				],
				"local" => [
					"ptbr" => get_field('type-eventos-ptbr-local', $post->ID),
					"enus" => get_field('type-eventos-enus-local', $post->ID)
				]
			];
		?>
		<div class="item">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-xs-3">
						<time class="data">
							<?php echo dateMouthLang($content['data'][$lang],$lang); ?>
						</time>
					</div>
					<div class="col-md-5 col-xs-9">
						<img class="img-response" src="<?php echo $content['imagem']['sizes']['eventos'];?>"/>
					</div>
					<div class="col-md-4 col-xs-9 col-md-offset-0 col-xs-offset-3">
						<div class="contenttext">
							<h1><?php echo $content['titulo'][$lang];?></h1>
							<div class="info">
								<span class="item">
									<?php echo $state['content']['local'][$lang];?>
									<span class="val"><?php echo $content['local'][$lang];?></span>
								</span>
								<span class="item">
									<?php echo $state['content']['horario'][$lang];?>
									<span class="val"><?php echo $content['horario'][$lang];?></span>
								</span>
							</div>
			              <p><?php echo reduzirTexto($content['conteudo'][$lang], 20, ''); ?></p>
			              <a href="<?php the_permalink();?>" class="leiamais">Leia mais +</a>
			            </div>
					</div>
				</div>
			</div>
		</div>

		<?php endwhile;wp_reset_postdata();?>


		<div class="paginacao">
			<div class="paginacao-items"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 h--txt-center">
						<button class="button--base-big button--gd-hs1 paginacao--botao" data-lista="<?=$lista?>" data-lang="<?=$lang?>" data-regra="Eventos" data-requisita="eventos" data-requisitapagina="2">ver mais</button>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

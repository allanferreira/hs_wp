<section class="orcamentopassagenshaView">
  <div class="nav--menu-height-single"></div>
  <?php
	$content = [];
	$cats = [get_cat_ID('ha'), get_cat_ID('orcamentopassagens')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"titulo" => [
				"ptbr" => get_field('ha-orcamentopassagens-ptbr-titulo', $post->ID),
				"enus" => get_field('ha-orcamentopassagens-enus-titulo', $post->ID)
			],
			"form" => [
				"ptbr" => get_field('ha-orcamentopassagens-ptbr-form', $post->ID),
				"enus" => get_field('ha-orcamentopassagens-enus-form', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content
	], 'orcamentopassagens', LANGUAGE, 'Views/HA/Orcamentopassagens/Parts');
	?>
</section>

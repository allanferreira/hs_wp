<section class="orcamentointercambiohaView">
  <div class="nav--menu-height-single"></div>
  <?php
	$content = [];
	$cats = [get_cat_ID('ha'), get_cat_ID('orcamentointercambio')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
			"titulo" => [
				"ptbr" => get_field('ha-orcamentointercambio-ptbr-titulo', $post->ID),
				"enus" => get_field('ha-orcamentointercambio-enus-titulo', $post->ID)
			],
			"form" => [
				"ptbr" => get_field('ha-orcamentointercambio-ptbr-form', $post->ID),
				"enus" => get_field('ha-orcamentointercambio-enus-form', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>

  <?php
	$component = new Component;
	$component->create([
		"content" => $content
	], 'orcamentointercambio', LANGUAGE, 'Views/HA/Orcamentointercambio/Parts');
	?>
</section>

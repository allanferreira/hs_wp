$('.carouselList .owl-carousel').owlCarousel({
  loop:true,
  margin:76,
  nav:false,
  responsive:{
    0:{
      items:1
    },
    600:{
      items:3
    }
  }
})
$('.gallery--list .owl-carousel').owlCarousel({
  loop:true,
  margin:76,
  nav:false,
  responsive:{
    0:{
      items:1
    },
    767:{
      items:3
    }
  }
})
$('.carouselList .iconprev').click(function(){
    $('.carouselList .owl-carousel .owl-prev').trigger('click')
})
$('.carouselList .iconnext').click(function(){
    $('.carouselList .owl-carousel .owl-next').trigger('click')
})
$('.gallery--list__icon.iconprev').click(function(){
    $('.gallery--list .owl-prev').trigger('click')
})
$('.gallery--list__icon.iconnext').click(function(){
    $('.gallery--list .owl-next').trigger('click')
})

<div class="ha-aaustralia-detalhes">
  <div class="cidadeDetalheImage" style="background-image:url(<?=$state['image']['sizes']['cidadeDetalhe']?>)"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 h--table">
        <div class="cidadeDetalhe">
          <?=$state['content'][$lang]?>
        </div>
      </div>
    </div>
  </div>
</div>

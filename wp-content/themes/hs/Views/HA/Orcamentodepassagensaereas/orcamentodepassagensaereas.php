<div class="orcamentodepassagensaereasView">
  <div class="nav--menu-height-single"></div>
	<?php
	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('orcamentodepassagensaereas')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

		$content = [
  		"titulo" => [
  			"ptbr" => get_field('hs-orcamentodepassagensaereas-orcamentodepassagensaereas-ptbr-titulo', $post->ID),
  			"espe" => get_field('hs-orcamentodepassagensaereas-orcamentodepassagensaereas-espe-titulo', $post->ID)
  		],
  		"form" => [
				"ptbr" => get_field('hs-orcamentodepassagensaereas-orcamentodepassagensaereas-ptbr-formulario', $post->ID),
				"espe" => get_field('hs-orcamentodepassagensaereas-orcamentodepassagensaereas-espe-formulario', $post->ID)
			]
		];

	endwhile; wp_reset_postdata();
	?>

	<?php
	$component = new Component;
	$component->create([
		"content" => $content,
	], 'orcamentodepassagensaereas', LANGUAGE, 'Views/HA/Orcamentodepassagensaereas/Parts');
	?>
</div>

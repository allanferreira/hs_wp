<section class="section--text hs-sobre-parceiros">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
					<span></span>
				</div>
				<div class="content">
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="gallery">
					<?php 
					$cats = [get_cat_ID('hs'), get_cat_ID('sobre')];
					$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
					while ( $query->have_posts() ): $query->the_post();
						$images = get_field('hs-sobre-parceiros-galeria');
						foreach ($images as $image):
					?>
						<li class="item">
							<a href="#" target="_blank">
								<img src="<?php echo $image['url'];?>"/>
							</a>
						</li>

					<?php endforeach; endwhile; wp_reset_postdata();?>
				</ul>
			</div>
		</div>
	</div>
</section>
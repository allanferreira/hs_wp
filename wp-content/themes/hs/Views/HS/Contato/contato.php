<div class="contatoView">
  <div class="nav--menu-height"></div>
	<?php

	$content = [];
	$cats = [get_cat_ID('hs'), get_cat_ID('contato')];
	$query = new WP_Query(['post_type'=>'page','category__and' => $cats]);
	while ( $query->have_posts() ): $query->the_post();

    $content = [
      "hs-contato-contato" => [
        "titulo" => [
          "ptbr" => get_field('hs-contato-contato-ptbr-titulo', $post->ID),
          "enus" => get_field('hs-contato-contato-enus-titulo', $post->ID)
        ],
        "conteudo" => [
          "ptbr" => get_field('hs-contato-contato-ptbr-conteudo', $post->ID),
          "enus" => get_field('hs-contato-contato-enus-conteudo', $post->ID)
        ],
        "nossasunidades" => [
          "ptbr" => get_field('hs-contato-contato-ptbr-nossasunidades', $post->ID),
          "enus" => get_field('hs-contato-contato-enus-nossasunidades', $post->ID)
        ]
      ],
      "hs-contato-formulario" => [
        "ptbr" => get_field('hs-contato-formulario-ptbr-assuntoemail', $post->ID),
        "enus" => get_field('hs-contato-formulario-enus-assuntoemail', $post->ID)
      ],
      "hs-contato-nossasunidades" => [
        "ptbr" => get_field('hs-contato-nossasunidades-ptbr-enderecos', $post->ID),
        "enus" => get_field('hs-contato-nossasunidades-enus-enderecos', $post->ID)
      ]
    ];

	endwhile; wp_reset_postdata();
	?>


  <?php
  $component = new Component;
  $component->create([
    "hs-contato-contato" => $content["hs-contato-contato"],
    "hs-contato-nossasunidades" => $content["hs-contato-nossasunidades"]
  ], 'nossas-unidades', LANGUAGE, 'Views/HS/Contato/Parts');
  ?>
	<?php
  $component = new Component;
	$component->create([
		"hs-contato-contato" => $content["hs-contato-contato"],
		"hs-contato-formulario" => $content["hs-contato-formulario"],
    "statics" => [
      "assunto" => [
        "ptbr"=>"Assunto",
        "enus"=>"Assunto"
      ],
      "escolhaumassunto" => [
        "ptbr"=>"Escolha um assunto",
        "enus"=>"Escolha um assunto"
      ],
      "nome" => [
        "ptbr"=>"Nome",
        "enus"=>"Nome"
      ],
      "email" => [
        "ptbr"=>"Email",
        "enus"=>"Email"
      ],
      "telefone" => [
        "ptbr"=>"Telefone",
        "enus"=>"Telefone"
      ],
      "mensagem" => [
        "ptbr"=>"Mensagem",
        "enus"=>"Mensagem"
      ],
      "enviar" => [
        "ptbr"=>"enviar",
        "enus"=>"enviar"
      ],
      "iframe" => [
        "ptbr" => "https://helloaustralia.1enrol.com/form/f1414d70-d54f-41bc-ac74-a6fe00f926b3",
        "enus" => "https://helloaustralia.1enrol.com/form/f1414d70-d54f-41bc-ac74-a6fe00f926b3"
      ]
    ]
	], 'formulario', LANGUAGE, 'Views/HS/Contato/Parts');
	?>
</div>

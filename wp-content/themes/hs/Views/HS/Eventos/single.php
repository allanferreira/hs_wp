<div class="eventosSingleView">
	<div class="nav--menu-height"></div>
  <?php
  the_post();
  $content = [
    "titulo" => [
      "ptbr" => get_field('type-eventos-ptbr-titulo', $post->ID),
      "enus" => get_field('type-eventos-enus-titulo', $post->ID)
    ],
    "local" => [
      "ptbr" => get_field('type-eventos-ptbr-local', $post->ID),
      "enus" => get_field('type-eventos-enus-local', $post->ID)
    ],
    "telefone" => [
      "ptbr" => get_field('type-eventos-ptbr-telefone', $post->ID),
      "enus" => get_field('type-eventos-enus-telefone', $post->ID)
    ],
    "rua" => [
      "ptbr" => get_field('type-eventos-ptbr-rua', $post->ID),
      "enus" => get_field('type-eventos-enus-rua', $post->ID)
    ],
    "cidade" => [
      "ptbr" => get_field('type-eventos-ptbr-cidade', $post->ID),
      "enus" => get_field('type-eventos-enus-cidade', $post->ID)
    ],
    "estado" => [
      "ptbr" => get_field('type-eventos-ptbr-estado', $post->ID),
      "enus" => get_field('type-eventos-enus-estado', $post->ID)
    ],
    "cep" => [
      "ptbr" => get_field('type-eventos-ptbr-cep', $post->ID),
      "enus" => get_field('type-eventos-enus-cep', $post->ID)
    ],
    "site" => [
      "ptbr" => get_field('type-eventos-ptbr-site', $post->ID),
      "enus" => get_field('type-eventos-enus-site', $post->ID)
    ],
    "extra" => [
      "ptbr" => get_field('type-eventos-ptbr-extra', $post->ID),
      "enus" => get_field('type-eventos-enus-extra', $post->ID)
    ],
    "data" => [
      "ptbr" => get_field('type-eventos-ptbr-data', $post->ID),
      "enus" => get_field('type-eventos-enus-data', $post->ID)
    ],
    "horario" => [
      "ptbr" => get_field('type-eventos-ptbr-horario', $post->ID),
      "enus" => get_field('type-eventos-enus-horario', $post->ID)
    ],
    "conteudo" => [
      "ptbr" => get_field('type-eventos-ptbr-conteudo', $post->ID),
      "enus" => get_field('type-eventos-enus-conteudo', $post->ID)
    ]
  ];
  ?>

  <?php
  $component = new Component;
  $component->create([
  	"content" => $content,
		"statics" => [
			"local" => [
				"ptbr"  => "Local",
				"enus"  => "Local",
				"eses"  => "Sitio"
			],
			"horario" => [
				"ptbr"  => "Horário",
				"enus"  => "Schedule",
				"eses"  => "Tiempo"
			],
			"data" => [
				"ptbr"  => "Data",
				"enus"  => "Data",
				"eses"  => "Data"
			],
			"cidade" => [
				"ptbr"  => "Cidade",
				"enus"  => "Cidade",
				"eses"  => "Cidade"
			]
		]
  ], 'evento', LANGUAGE, 'Views/HS/Eventos/Partssingle');
  ?>
</div>

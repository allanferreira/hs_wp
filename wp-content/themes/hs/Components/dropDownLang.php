<div class="dropdownlang">
	<div class="dropdownlang__view">
		<span class="dropdownlang__view__current">
			<div class="bandeira brasil"></div>
			<?php echo $state['content']['brasil'][$lang];?>
			<div class="bandeira-arrow"></div>
		</span>
	</div>
	<ul class="dropdownlang__drop">
		<a href="?lg=ptbr">
			<li>
					<div class="bandeira brasil"></div>
					<?php echo $state['content']['brasil'][$lang];?>
					<div class="bandeira-arrow"></div>
			</li>
		</a>
		<a href="?lg=esco">
			<li>
					<div class="bandeira colombia"></div>
					<?php echo $state['content']['colombia'][$lang];?>
					<div class="bandeira-arrow"></div>
			</li>
		</a>
		<a href="?lg=espe">
			<li>
					<div class="bandeira peru"></div>
					<?php echo $state['content']['peru'][$lang];?>
					<div class="bandeira-arrow"></div>
			</li>
		</a>
		<a href="?lg=enus">
			<li>
					<div class="bandeira america"></div>
					<?php echo $state['content']['america'][$lang];?>
					<div class="bandeira-arrow"></div>
			</li>
		</a>
	</ul>
</div>


$('.formtarget').click(function(){
  const target = $(this).data('target')
        , res = `<iframe src="${target}"></iframe>`

  $('.formtarget')
    .removeClass('box-enable')
    .addClass('box-disable')

  $(this)
    .removeClass('box-disable')
    .addClass('box-enable')

  $('.formresult').html(res)
})

let get = document.location.href.split('?')[1]
if(get) $('.formtarget.'+get).trigger('click')

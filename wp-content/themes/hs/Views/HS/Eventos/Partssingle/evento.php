<?php
$local = $state['content']['local'][$lang];
$rua = $state['content']['rua'][$lang];
$cidade = $state['content']['cidade'][$lang];
$estado = $state['content']['estado'][$lang];
$cep = $state['content']['cep'][$lang];
$telefone = $state['content']['telefone'][$lang];
$site = $state['content']['site'][$lang];
$extra = $state['content']['extra'][$lang];
$horario = $state['content']['horario'][$lang];
$data = $state['content']['data'][$lang];
?>
<section class="section--text hs-single-evento">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="title">
					<h1><?php echo $state['content']['titulo'][$lang];?></h1>
				</div>
				<div class="vitrine">
          <div class="info">
              <div class="base">
                <?php if(!empty($state['content']['cidade'][$lang])):?>
                  <strong><?php echo $state['statics']['cidade'][$lang];?>:</strong>
                  <?php echo $state['content']['cidade'][$lang];?><br/>
                <?php endif;?>

                <strong><?php echo $state['statics']['data'][$lang];?>:</strong>
                <time><?php echo $data; ?></time><br/>

                <strong><?php echo $state['statics']['horario'][$lang];?>:</strong>
                <span class="val"><?php echo $horario;?></span>
              </div>
              <div class="address">
                <h1><?php echo $local;?></h1>
								<p>
                  <?php echo $extra;?>
                  <?php if(!empty($rua)):?>
	                  <?php echo $rua;?>, <?php echo $cidade;?> - <?php echo $estado;?>, <?php echo $cep;?><br/>
                  <?php endif;?>
	                <?php echo $telefone;?><br/>
	                <?php echo $site;?>
								</p>
              </div>
          </div>
          <div>
            <iframe class="mapa" src="<?php echo montaUrlMapa($rua, $local, $cidade, $estado, $cep);?>"></iframe>
          </div>
        </div>
        <div class="content">
					<p><?php echo $state['content']['conteudo'][$lang];?></p>
				</div>
			</div>
		</div>
	</div>
</section>
